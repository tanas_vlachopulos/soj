/********************
*   SOJ cv 03
*   Tasks:
*   1. Search minimum and maximum in array of integers. Do not use conditional jumps, use CMOVcc (and LOOP).
*   2. Compute how many bits are set to 1 in integer variable. Use idiv.
*   3. Convert string to uppercase, use conditional jumps.
*   4. Convert string to uppercase, use CMOVcc, conditional jumps can be used only for string termination detection.
********************/
#include <stdio.h>

// Variables
int array[10] = {500, 10, 3, 4, -5, -6, 7, 8, 9, 112};
int length = 10;
int result = 0;
int min, max;
int bit_count;
int number = 255;
char string1[] = "hello_WORLD{}";
char string2[] = "don't panic {}";

// function prototypes
void prints();
void experiments();
void find_min_max();
void compute_bits();
void to_uppercase();
void to_uppercase_cmov();

int main()
{
    experiments();
    find_min_max();
    compute_bits();
    to_uppercase();
    to_uppercase_cmov();

    prints();
}

void prints()
{
    printf("result=%d\n", result);
    printf("min=%d  max=%d\n", min, max);
    printf("Count of bits=%d\n", bit_count);
    printf("string1= %s\nstring2= %s\n", string1, string2);
}
