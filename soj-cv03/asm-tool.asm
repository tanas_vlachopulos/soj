;***************************************************************************
;
; Examples and tasks from CV 03
; VLA0054
;
;***************************************************************************
	bits 32

	section .data
; variables
extern array
extern result
extern length
extern min, max
extern bit_count, number
extern string1, string2

        section .text
; functions
global find_min_max
global experiments
global compute_bits
global to_uppercase
global to_uppercase_cmov

; labels:
experiments:
	;mov ebx, dword [ array + 0 ]
	xor edx, edx
	mov eax, dword [array]
	mov ebx, dword [array + 1 * 4]
	cmp dword eax, dword ebx
	cmovl edx, dword eax
	mov [result], dword edx
	;
	ret


;;; TASK 1
;;; search min and max in array using CMOV
find_min_max:
	mov eax, dword [array]
	mov ecx, [length]
	mov [min], eax
	;mov [max], eax
.loop_lable:
	mov eax, dword [array + (ecx - 1) * 4]
	mov ebx, dword [min]
	cmp eax, ebx				; if array[i] < min
	cmovl ebx, eax				; then min = array[i]
	mov [min], dword ebx
	;
	mov ebx, dword [max]
	cmp eax, ebx				; if array[i] > max
	cmovg ebx, eax				; then min = array[i]
	mov [max], dword ebx
	;
	loop .loop_lable
	;
	ret

;;; TASK 2
;;; Compute how many bits are set to 1 in integer variable
compute_bits:
	mov eax, dword [number]
.loop_lable:
	mov ebx, dword 2
	cdq
	idiv ebx					; number = number / 2; edx = number % 2
	;
	mov ebx, [bit_count]
	add ebx, edx				; bit_count += number %2
	mov [bit_count], ebx
	;							; if (number != 0) then jump .loop_lable
	cmp eax, dword 0
	jnz .loop_lable
	;
	ret

;;; TASK 3
;;; Convert string to uppercase
to_uppercase:
	mov ecx, dword 0			; init ECX register
	xor al, al					; clear lower part of acumulator
.loop_lable:
	mov al, [string1 + ecx]		; get char from string
	;
	cmp al, 'a'					; test if leter is lowercase alphabet
	jb .skip_char
	cmp al, 'z'
	ja .skip_char
	;
	sub al, byte 32				; do uppercase; string1[ecx] = string1[ecx] - ('a' -'A');
	mov [string1 + ecx], al
	;
.skip_char:
	inc ecx						; ecx++
	cmp al, 0					; test end of string
	jne .loop_lable				; if (ecx < string1.length()) then jump .loop_lable
	;
	ret

;;; TASK 4
;;; Conver string to uppercase with CMOV
to_uppercase_cmov:
	mov ecx, dword 0
	xor al, al
.loop_lable:
	mov al, [string2 + ecx]
	mov bx, 32					; upper and lower case offset
	mov dx, 0
	;
	cmp al, 'a'					; test if char is lower case alphabet
	cmovb bx, dx				; if not clear offset register
	cmp al, 'z'
	cmova bx, dx
	;
	sub al, bl					; string2[ecx] = string2[ecx] - offset
	mov [string2 + ecx], al
	;
	inc ecx						; ecx++
	cmp al, 0					; test end of string
	jne .loop_lable				; if (ecx < string1.length()) then jump .loop_lable
	;
	ret