;***************************************************************************
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 32

	section .data

        section .text

        global strcompare
strcompare:
        enter 0,0
        push edi
        push esi

        push dword [ ebp + 8 ]      ; s1
        call strlength
        add esp, 4
        push eax

        push dword [ ebp + 12 ]     ; s2
        call strlength
        add esp, 4
        pop ecx
        cmp ecx, ebx
        cmovl ecx, eax              ; min( eax, ecx )

        mov ax, ds
        mov es, ax

        mov edi, [ ebp + 8 ]        ; s1
        mov esi, [ ebp + 12 ]       ; s2
repe    cmpsb
        mov eax, 0
        mov esi, -1
        mov edi, 1
        cmovb eax, edi
        cmova eax, esi, 

        pop esi
        pop edi
        leave
        ret

        global strlength
strlength:
        enter 0,0
        push edi

        mov edi, [ ebp + 8 ]        ; str
        mov al, 0                   ; terminator
        mov ecx, 0 ; 0xffffffff
        dec ecx

repne   scasb

        sub edi, [ ebp + 8 ]
        dec edi

        mov eax, edi

        pop edi
        leave
        ret

        global namala
namala:
        enter 0,0
        push edi
        push esi

        mov ax, ds
        mov es, ax
        ;cld                        ; default df=0
        mov edi, [ ebp + 8 ]        ; str
        mov esi, edi
.back:
        lodsb                       ; al=*(esi++)
        cmp al, 0                   ; terminator?
        je .hotovo

        cmp al, 'A'
        jb .taknic
        cmp al, 'Z'
        ja .taknic
        add al, 'a' - 'A'
.taknic:
        stosb
        jmp .back
.hotovo:
        mov eax, [ ebp + 8 ]

        pop esi
        pop edi
        leave
        ret

        global add64
add64:
        enter 0,0

        mov eax, [ ebp + 8 ]        ; a_low
        add eax, [ ebp + 16 ]       ; a_low+=b_low
        mov edx, [ ebp + 12 ]       ; a_high
        adc edx, [ ebp + 20 ]       ; a_high+=b_high

        leave
        ret
