
#include <stdio.h>

int sum32(int a, int b);
long sum64(long a, long b);
long sum_int(int *array, int size);
long sum_long(long *array, int size);	
long asm_strlen(char *str);	

// tasks
long infinit_sum64(int, ...);
long array_mean(long *array, int size);
void mult_int_char(int *int_arr, char *char_arr, int size);

int main()
{
	// *** tasks ***
	
	// task 1.2
	printf("infinit_sum64 %ld \n", infinit_sum64(1,1,1,1,1,1,1,1,1,1,10,0));

	// task 2
	long array1[5] = {-1000000000,-1000000000,-1000000000,-100000000,-10000000000};
	printf("prumer pole: %ld \n", array_mean(array1, 5));

	// task 3
	int array_int[10] = {1, 2, 3, 10, 20, 30, 100, 200, 300, 31415};
	char array_char[10] = {2,2,2,2,2,2,2,2,2,2};
	mult_int_char(array_int, array_char, 10);
	for (int i = 0; i < 10; i++) {
		printf("%d ", array_int[i]);
	}
	printf("\n");

	// examples
	printf("\n");
	printf("soucet 32 %d \n", sum32(1000, 33333333));
	printf("soucet 32 %d \n", sum32(-1000, 33333333));
	printf("soucet 32 %d \n", sum32(-1000, -3333333));
	printf("soucet 64 %ld \n", sum64(1000, 3333333333));

	int array[10] = {1, 2, 3, 10, 20, 30, 100, 200, 300, 31415};
	printf("soucet int %ld \n", sum_int(array, 10));

	long larray[10] = {1, 2, 3, 10, 20, 30, 100, 200, 300, 31415};
	printf("soucet long %ld \n", sum_long(larray, 10));

	printf("delka retezce %ld \n", asm_strlen("hola hola assembler vola"));
}
