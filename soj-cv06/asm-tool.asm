	bits 64

	section .data

	form	db	'tisk z asm %ld', 10, 0

	section .text

	global sum32, sum64
	extern printf

;;;;;;;;;;;;;
;;; TASKS ;;;
;;;;;;;;;;;;;

;;; TASK 1.1
	global infinit_sum32
infinit_sum32:

	ret

;;; TASK 1.2
; parametry RDI, RSI, RDX, RCX, R8 a R9
	global infinit_sum64
infinit_sum64:
	enter 0,0
	xor eax, eax

	; RDI
	cmp rdi, 0
	je .done
	add rax, rdi
	; RSI
	cmp rsi, 0
	je .done
	add rax, rsi
	; RDX
	cmp rdx, 0
	je .done
	add rax, rdx
	; RCX
	cmp rcx, 0
	je .done
	add rax, rcx
	; R8
	cmp r8, 0
	je .done
	add rax, r8
	; R9
	cmp r9, 0
	je .done
	add rax, r9

	mov rcx, 0
.while:
	mov rdx, [rbp + 16 + rcx]
	cmp rdx, 0
	je .done
	add rax, rdx

	add rcx, 8
	jmp .while

.done:
	leave
	ret

;;; Task 2
	global array_mean
array_mean:
	mov rcx, rsi		; rcx = size
	xor rax, rax

.back:
	add rax, [ rdi + rcx * 8 - 8 ]
	adc rdx, 0
	loop .back

	cqo
	idiv rsi

	ret

;;; Task 3
	global mult_int_char
mult_int_char:
	mov rcx, rdx
	
.back:
	movsx rax, dword [rdi + rcx * 4 - 4]
	movsx rdx, byte [rsi + rcx * 1 - 1]

	imul rdx

	mov [rdi + rcx * 4 - 4], dword eax

	loop .back

	ret

;;; Examples ;;;

; funkce s 2 vstupnimi body
sum32:
sum64:
	; protoze se nepredava zasobnikem nemusime pouzivat enter a leave
	mov rax, rdi	; rax = a
	add rax, rsi	; rax = rax + b

	ret

;;; sum 32 bit array
	global sum_int
sum_int:
	mov rcx, rsi	; rcx = size
	; movsx rcx, esi	; rozhtahne 32 bitovy registr na 64 bitovy, tato konverze ovsem funguje implicitne i u obycejneho mov
	xor rax, rax	; clear rax

.back:
	movsx r8, dword [ rdi + rcx * 4 - 4]	; r8 = array[ecx]; prochazim polem intu, takze musim postupovat po 4 bytech
	add rax, r8
	loop .back

	ret

;;; sum long array
	global sum_long
sum_long:
	mov rcx, rsi		; rcx = size
	xor rax, rax

.back:
	add rax, [ rdi + rcx * 8 - 8 ]
	loop .back

	; call prinf
	push rax			; zalohuju rax pred volanim funkce
	mov rdi, form		; prvni vstupni parametr pro printf; retezec je pointer takze se predava bez []
	mov rsi, rax		; druhy vstupni parametr pro printf
	mov al, 0			; musi se nulovat u funkci s promennym poctem parametru jako je prinf
	call printf			; printf(form, rax)
	pop rax				; vytahnu rax z uschovy

	ret

;;;	strlen
	global asm_strlen
asm_strlen:
	mov ax, ds
	mov es, ax

	mov rsi, rdi		; zaloha zacatku retezce
	mov rcx, -1			; maximalni mozna delka
	mov al, 0

	repne scasb
	sub rdi, rsi		; odecteni nalezeneho konce od predem ulozeneho zacatku len = end - begin

	mov rax, rdi		; rax = len
	dec rax				; scasb vzdy spocita o jednu hodnotu vice

	ret