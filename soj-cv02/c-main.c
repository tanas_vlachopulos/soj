//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>

// Variables
int a, b, c, d, e, f; 
int mult, ratio, reminder;
int array[10] = {2, 2, 3, 4, -5, 6, 7, 8, 9, 12};
int remainder_array[10];
int sum;
int length = 10;
int max, min;
int odd_sum, even_sum;
int multiplicator = 5;
int factor = 6;

void operation_test();
void sum_array();   
void find_min();
void find_max();
void multiply_array();
void sum_of_odd_and_even();
void count_remainders();

int main()
{
    a = 5;
    b = 10;
    c = 40;
    d = 42;
    // operation_test();
    // sum_array();
    find_min();
    find_max();
    sum_of_odd_and_even();

    printf("a=%d, b=%d, c=%d, d=%d, e=%d, f=%d \n", a, b, c, d, e, f);
    printf("mult=%d\n", mult);
    printf("ratio=%d, reminder=%d\n", ratio, reminder);
    printf("length=%d, sum=%d\n", length, sum);
    printf("min=%d max=%d\n", min, max);
    printf("odd sum = %d, even sum = %d\n", odd_sum, even_sum);

    multiply_array();
    printf("Multiplied array:   ");
    for (int i = 0; i < 10; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");

    count_remainders();
    printf("Reminders: ");
    for (int i = 0; i < 10; i++)
    {
        printf("%d ", remainder_array[i]);
    }
    printf("\n");
}
