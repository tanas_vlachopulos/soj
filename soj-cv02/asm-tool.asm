;***************************************************************************
;
; Examples and tasks from CV 02
; VLA0054
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 32

	section .data
; variables
; global
; externs
extern a, b, c, d, e, f, mult, ratio, reminder
extern sum, array, length
extern max, min
extern multiplicator
extern odd_sum, even_sum
extern factor, remainder_array
; tmp_sum
tmp_sum			dd	0
tmp_sum_even	dd	0

        section .text
; functions
global operation_test
global sum_array
global find_min
global find_max
global multiply_array
global sum_of_odd_and_even
global count_remainders
; global
; extern

; labels:
operation_test:
	; a = a + b
	; nelze primo scitat dve adresy, musi se proto ulozit cast do akumulatoru
	mov eax, [b]
	add [a], eax
	;
	; e = c - d
	mov eax, [d] ; presune promenou d do aku
	mov [e], eax ; napni e akumulatorem
	mov eax, [d] ; do akumulatoru presune d
	sub [e], eax ; od e odecte aku
	;
	mov [f], eax
	inc dword [f]
	;
	; a * b
	mov eax, [a]
	imul dword [b]
	mov [mult], eax
	;
	mov eax, [mult]
	cdq					; pripravi EAX pro zaporne cislo
	idiv dword [d]		; deleni
	mov [ratio], eax	; vysledek deleni
	mov [reminder], edx	; zbytek po deleni
	;
	ret

sum_array:
	mov ecx, [length]	; priradi delku pole do citaciho registru
	mov eax, 0			; docasny soucet
	xor eax, eax		; vyplni cely eax s 0, je mozne udelat i jinymi zpusoby, treba sub eax, eax
.tmp_label:				; lokalni navesti	
	cmp [ array + (ecx - 1) * 4 ], dword 0	; porovna hodnotu v poli s cislem 0
	jle .skip_num		; skoci na skip_num pokud je hodnota v poli mensi nez 0
	; indexovani pole
	; od ecx se odecita 1, protoze v C jsou pole indexovane od nuly, ale v ASM skonci cyklus na indexu 1, 0 cyklus ukoncuje
	add eax, [ array + (ecx - 1) * 4 ]
.skip_num:
	loop .tmp_label
	mov [sum], eax		; ulozi vysledek do promenne sum
	;
	ret

;;; find min value of array
find_min:
	mov eax, [ array ]
	mov ecx, [length]
.tmp_label:
	cmp [ array + (ecx - 1) * 4], dword eax
	jge .min_label 
	mov eax, [ array + (ecx - 1) * 4]
.min_label:
	loop .tmp_label
	mov [min], eax
	;
	ret


;;; find max value 
find_max:
	mov eax, [ array ]
	mov ecx, [ length ]
.tmp_label:
	cmp [ array + (ecx - 1) * 4], dword eax
	jle .max_label 
	mov eax, [ array + (ecx - 1) * 4]
.max_label:
	loop .tmp_label
	mov [max], eax
	;
	ret

;;; multiply array
multiply_array:
	mov ecx, [ length ]
.loop_label:
	mov eax, [multiplicator]
	imul dword [ array + (ecx - 1) * 4]
	mov [ array + (ecx - 1) * 4], eax
	loop .loop_label
	;
	ret

;;; Task 3
;;; make sum of odd and even numbers in array
;;;
sum_of_odd_and_even:
	mov ecx, [length]							; set counter to length of array
.loop_label:
	mov eax, dword [ array + (ecx - 1) * 4]		; set acc to element of array
	mov ebx, dword 2							; set edx 
	cdq											; adjust register to signet numbers
	idiv ebx									; eax = eax / ebx;	edx = eax % ebx;
	;
	cmp edx, dword 0							; if (edx != 0) jump .not_odd;
	jne .not_odd
	;											; odd numbers block
	; 											;tmp_sum += array[ecx];
	mov eax, [tmp_sum]							
	add eax, [ array + (ecx - 1) * 4]
	mov [tmp_sum], eax
	;
	jmp .not_even								; skip block for even numbers
	;
.not_odd:										; even numbers block
	; 											; tmp_sum_even += array[ecx];
	mov eax, [tmp_sum_even]
	add eax, [ array + (ecx - 1) * 4]
	mov [tmp_sum_even], eax
	;
.not_even:										; end of cycle block
	loop .loop_label
	mov eax, [tmp_sum]							; odd_sum = tmp_sum
	mov [odd_sum], eax
	;											; even_sum = tmp_sum_even
	mov eax, [tmp_sum_even]
	mov [even_sum], eax
	;
	ret

;;; Task 4
;;; compute remainder of element and store it to array remainder_array
;;;
count_remainders:
	mov ecx, [length]
.loop_label:
	mov eax, dword [array + (ecx - 1) * 4]		; load element of original array
	mov ebx, [factor]							; load factor
	cdq											; prepare registers for division
	idiv ebx									; eax = eax / ebx; edx = eax % ebx
	mov [remainder_array + (ecx - 1) * 4], edx	; save remainder to new remainder_array
	;
	loop .loop_label
	;
	ret