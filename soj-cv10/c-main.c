/*
*	TASKS:
*	1] kvadraticka rovnice
*	2] odcitani float cisel s pomoci unionu
*/
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define Z 16

int fix_mul(int a, int b);
int fix_div(int a, int b);

// simple 32bit sqrt
int sqrt_int32( int a )
{
	long long delenec = a;
	int *scena = ( int * ) &delenec;
	scena++;
	int res = 0;

	for ( int i = 0; i < 16; i++ )
	{
		res <<= 1;
		delenec <<= 2;
		int B = res * 2 * 1 + 1 * 1;
		if ( *scena >= B )
		{
			res |= 1;
			*scena -= B;
		}
	}
	return res;
}

float fix2float(int fix)
{
	float ret = fix;
	ret /= (1 << Z);
	return ret;
}

int float2fix(float fl)
{
	return fl * (1 << Z);
}

union pitva
{
	float fl;
	struct
	{
		unsigned int m:23;
		unsigned int e:8;
		unsigned int s:1;
	};
};

union cislo
{
	int fix;
	struct 
	{
		unsigned int m:23;
		unsigned int m1:9; // 1 na zacatku desetineho cisla
	};
};

void mul_float(float fa, float fb)
{
	union pitva a, b, c;
	union cislo ca, cb, cc;

	a.fl = fa;
	b.fl = fb; 
	c.s = ( a.s + b.s ) % 2; // .s je signum znamenko - tohle resi znamenka pri operacemi mezi pa a pb
	ca.m = a.m;
	cb.m = b.m;
	ca.m1 = 1;
	cb.m1 = 1;
	long long tmp = ca.fix;
	cc.fix = ( tmp * cb.fix ) >> 23; // posunuti o velikost biasu 
	c.e = a.e + b.e - 127;
	if ( cc.m1 >= 2 )
	{
		c.e++;
		cc.fix >>= 1;
	}
	c.m = cc.m;

	printf("c m:%d e:%d s:%d\n", c.m, c.e, c.s);
	printf( "cislo %f\n", c.fl );
	printf("-------------------\n");
}

// TASK 2 - scitani float cisel
float add_float(float fa, float fb)
{
	union pitva a, b, c;
	union cislo ca, cb, cc;

	a.fl = fa;
	b.fl = fb;

	if (a.e > b.e) // kvuli symetricnosti musi mit prvni mensi nebo stejnou hodnotu nez druhe cislo (na znamenkach nezalezi)
	{
		printf("first number must have smaller exponent than second num!\n");
		return 0;
	}

	printf("= %f + %f\t=\t", fa, fb);

	ca.m = a.m;
	cb.m = b.m;
	ca.m1 = 1;
	cb.m1 = 1;

	// DEBUG
	// printf("a m:%06x e:%d s:%d\n", ca.m, a.e, a.s);
	// printf("b m:%06x e:%d s:%d\n\n", cb.m, b.e, b.s);
	
	c.s = b.s; // znamenko z vetsiho cisla se pouzije i pro vysledek

	// normalizuje exponenty cisel - pokud byl exponent fb vtsi nez exponent fa, upravi cislo fa na stejny exponent jako fb
	int exp_shift = b.e - a.e;
	a.e += exp_shift;
	ca.fix >>= exp_shift;

	long long tmp = ca.fix;
	cc.fix = (b.s == a.s) ? tmp + cb.fix : cb.fix - tmp; // pokud jsou znamenka stejne provede soucet, v opacnem pripade odecte mensi od vetsiho
	c.e = a.e; // pouzije normalizovany exponent

	// DEBUG
	// printf("ca.m1=%06x, ca.m=%06x\ncb.m1=%06x, cb.m=%06x\n", ca.m1, ca.m, cb.m1, cb.m);
	// printf("cc.m1=%06x, cc.m=%06x\n", cc.m1, cc.m);
	
	if (cc.m1 >= 2) // v pripade ze cislo pretece do tvaru 10.10.. upravi do tvaru 1.010...
	{
		c.e++;
		cc.fix >>= 1;
	}
	// if (cc.m1 == 0) // upravi podteceni u odcitani 
	// {
	// 	c.e--;
	// 	cc.fix <<=1;
	// }
	c.m = cc.m;

	// DEBUG
	// printf("float %f\n", c.fl);
	// printf("exponent shift%d\n", exp_shift);
	// printf("pa m:%06x e:%d s:%d\n", c.m, c.e, c.s);
	// printf("pa m:%06x e:%d s:%d\n", ca.m, a.e, a.s);
	// printf("pb m:%06x e:%d s:%d\n\n", cb.m, b.e, b.s);
	// printf("********\n");
	return c.fl;
}

void quadratic_eaquation(float fa, float fb, float fc)
{
	int a = float2fix(fa);
	int b = float2fix(fb * (-1));
	int c = float2fix(fc);
	int const4 = float2fix(4);
	int const2 = float2fix(2);
	
	// discriminator
	int D = fix_mul(b, b) - fix_mul(fix_mul(const4, a), c);

	float fD = fix2float(D);
	printf("disc %f\n", fD); 

	long sqrD = sqrt_int32(D) * (1 << (Z/2));

	// float sqrDf = fix2float(sqrD);
	printf("sqrd disc %f\n", fix2float(sqrD));

	long x1 = fix_div(b + sqrD, fix_mul(const2, a));
	long x2 = fix_div(b - sqrD, fix_mul(const2, a));	

	printf("Roots x1=%f, x2=%f\n", fix2float(x1), fix2float(x2));
}

void test()
{
	 union pitva a, b, c;
    union cislo ca, cb, cc;
    
    a.fl = 13;
    b.fl = 5.58;    


if (a.e > b.e)
    {
		int diff = a.e - b.e;
		
		//b.s = b.s << diff;		
		ca.m = a.m;
		cb.m = b.m;
		ca.m1 = 1;
		cb.m1 = 1;
		
		b.e += diff;
		cb.fix >>= diff;
		c.s = 0;
		//cc.m = ca.m + cb.m;
		
		//cc.fix = ca.fix + cb.fix;
		
		
		long long tmp = ca.fix;
		if (b.s == a.s)
		{
				printf("test1\n");
				cc.fix = tmp + cb.fix;
			
		}
		else
		{
				printf("test2\n");
				cc.fix = cb.fix - tmp;
			
		}

		
		c.e = a.e;
		if ( cc.m1 >= 2 )
		{
			c.e++;
			cc.fix >>= 1;
		}
		
		
		
		c.m = cc.m;

		printf("result %f\n", c.fl);
	}
}

int main()
{
	// fix point test
	// int a = float2fix( 0.1 );
	// int b = float2fix( 0.01 );
	// int ab = fix_mul( a, b );
	// printf( "a= %08x %d\n", a, a );
	// printf( "b= %08x %d\n", b, b );
	// printf( "ab= %08x %d\n", ab, ab );
	// printf( "vysledek %f\n", fix2float( ab ) );
	// int tmp = fix_div( ab, b );
	// printf( "vysledek %f\n", fix2float( tmp ) );

	// float point
	union pitva pa, pb;
	pa.fl = 100;
	pb.fl = 3.3;

	printf("pa m:%d e:%d s:%d\n", pa.m, pa.e, pa.s);
	printf("pb m:%d e:%d s:%d\n\n", pb.m, pb.e, pb.s);

	pa.m = 0x300000; // 1.011 0000 0000 0000 ... cislo 1 na zacatku se nemusi zapisovat
	pa.e = 129;
	pa.s = 1;

	// mul_float(100, 3.3);
	// mul_float(2,2);
	// mul_float(2, 0.6);

	// printf("%f\n", add_float(2.2,2.2));
	// printf("%f\n", add_float(0.6,0.6));
	// printf("%f\n", add_float(1.1,1.1));
	// printf("%f\n", add_float(0.6,2.4));
	// printf("%f\n", add_float(-1,2));
	// printf("%f\n", add_float(-1,-1));
	// printf("%f\n", add_float(-2, -3));
	// printf("%f\n", add_float(2, -3));
	// printf("%f\n", add_float(-2,3));
	// printf("%f\n", add_float(3, -7));
	// printf("%f\n", add_float(15, -16));
	

	// quadratic_eaquation(2.0, 5.0, -5.0);	

	test();
}
