	bits 32

	section .data

		section .text

%define Z 16

	global fix_mul
fix_mul:
	enter 0,0
	mov eax, [ebp + 8]
	imul dword [ebp + 12]

	shrd eax, edx, Z

	leave
	ret

		global fix_div
fix_div:
	enter 0,0
	mov eax, [ ebp + 8 ]
	cdq
	shld edx, eax, Z
	shl eax, Z
	idiv dword [ ebp + 12 ]
	leave
	ret
