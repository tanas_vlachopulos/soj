#include <strings.h>
#include <string.h>
#include <stdio.h>

#define SIZE 8

void add_intN_intN(int *aN, int *bN, int N);
void sub_intN_intN(int *aN, int *bN, int N);
int shift_left_intN(int *iN, int N);
int shift_right_intN(int *iN, int N);
int div_divN_int32(int *intN, int int32, int size);
int add_intN_int32(int *intN, int int32, int size);
int mul_intN_int32(int *intN, int int32, int size);

void str_mirror( char *str )
{
	int l = strlen( str );
	char tmp[ l + 1 ];
	for ( int i = 0; i < l; i++ )
		tmp[ l - i - 1 ] = str[ i ];
	tmp[ l ] = 0;
	strcpy( str, tmp );
}

int is_zero( int *intN, int N )
{
	for ( int i = 0; i < N; i++ )
		if ( intN[ i ] ) return 0;

	return 1;
}

// compare of two intN numbers
// a >  b return  1
// a == b return  0
// a <  b return -1
int cmp_intNintN( int *a, int *b, int N )
{
  int i;
  for ( i = N - 1; i >= 0; i-- )
  {
	  if ( a[ i ] == b[ i ] ) continue;
	  if ( ( unsigned int ) a[ i ] > ( unsigned int ) b[ i ] ) return 1;
	  return -1;
  }
  return 0;
}

void mul_intN_intN( int *aN, int *bN, int N )
{
	int res[ N ];
	bzero( res, sizeof( res ) );

	for ( int i = 0; i < N * 32; i++ )
	{
		if ( bN[ 0 ] & 1 )
		{
			add_intN_intN( res, aN, N );
		}
		shift_left_intN( aN, N );
		shift_right_intN( bN, N );
	}

	memcpy( aN, res, sizeof( res ) );
}

void div_intN_intN( int *aN, int *bN, int N )
{
	int divadlo[ 2 * N ];
	int res[ N ];
	bzero( res, sizeof( res ) );
	bzero( divadlo, sizeof( divadlo ) );
	memcpy( divadlo, aN, N * sizeof( int ) );
	int *scena = divadlo + N;

	for ( int i = 0; i < N * 32; i++ )
	{
		shift_left_intN( divadlo, 2 * N );
		shift_left_intN( res, N ); 
		if ( cmp_intNintN( scena, bN, N ) >= 0 )
		{
			res[ 0 ] |= 1;
			sub_intN_intN( scena, bN, N );
		}
	}
	memcpy( aN, res, N * sizeof( int ) );
	memcpy( bN, scena, N * sizeof( int ) ); // zbytek
}

int div_int32_int32( int a, int b )
{
	long long delenec = a;  // divadlo int[ 2 ]
	int *scena = ( int * ) &delenec;
	scena++;                // &int[ 1 ]
	int res = 0;

	for ( int i = 0; i < 32; i++ )
	{
		res <<= 1;
		delenec <<= 1;
		if ( *scena >= b )
		{
			res |= 1;
			*scena -= b;
		}

	}
	return res;
}

char *intN_to_str( int *intN, char *str, int N )
{
	char *tmp = str;
	while ( !is_zero( intN, N ) )
	{
		int zbytek = div_divN_int32( intN, 10, N );
		*str = zbytek + '0';
		str++;
	}
	*str = 0;

	str_mirror( tmp );
}

char *str_to_intN( char *str, int *intN, int N )
{
	for ( int i = 0; i < N; i++ )
		intN[ i ] = 0;
	while ( *str )
	{
		mul_intN_int32( intN, 10, N );
		add_intN_int32( intN, *str - '0', N );
		str++;
	}
}


int main()
{
	// add
	// add
	int num1[SIZE] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
	int num2[SIZE] = { 0xBEEFBEEF, 0x0 , 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
	add_intN_intN(num1, num2, SIZE);

	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num1[SIZE - 1 - i]);
	printf("\n");

	// sub
	int num3[SIZE] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
	sub_intN_intN(num3, num2, SIZE);

	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num3[SIZE - 1 - i]);
	printf("\n");

	
	// shift left
	// int num4[8] = { 0xBEEFBEEF, 0x22222222, 0x44444444, 0x88888888, 0xCAFECAFE, 0x0, 0x0, 0x0 };
	int num4[8] = { 0,0,0,0,0,0,0, 0xbeefbeef };	
	printf("Shift left:\nOriginal: ");
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num4[SIZE - 1 -i]);
	printf("\n");

	shift_left_intN(num4, SIZE);
	printf("Shifted:  ");	
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num4[SIZE - 1 -i]);
	printf("\n\n");

	// shift right
	int num5[8] = { 0xBEEFBEEF, 0x22222222, 0x44444444, 0x88888888, 0xCAFECAFE, 0x0, 0x0, 0x0 };
	printf("Shift Right:\nOriginal: ");
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num5[SIZE - 1 -i]);
	printf("\n");

	shift_right_intN(num5, SIZE);
	printf("Shifted:  ");	
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num5[SIZE - 1 -i]);
	printf("\n\n");

	// division
	int num6[SIZE] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
	int num7[SIZE] = { 0x10000, 0, 0, 0, 0, 0, 0, 0 };

	div_intN_intN( num6, num7, SIZE );

	printf("num6: ");	
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num6[SIZE - 1 -i]);
	printf("\n");
	printf("num7: ");	
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num7[SIZE - 1 -i]);
	printf("\n\n");

	// multiplication
	int num8[SIZE] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
	int num9[SIZE] = { 0x10000, 0, 0, 0, 0, 0, 0, 0 };

	mul_intN_intN( num8, num9, SIZE );

	printf("num8: ");	
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", num8[SIZE - 1 -i]);
	printf("\n\n");

	// POLI tests 
	// POLI tests 
	printf("POLI tests:\n");
	
	char sn1[ 100 ] = "123456789123456789123456789";
	char sn2[ 100 ] = "10000000000";
	char sres[ 100 ];

	int n1[ 8 ], n2[ 8 ];
	str_to_intN( sn1, n1, 8 );
	str_to_intN( sn2, n2, 8 );

	mul_intN_intN( n1, n2, 8 );
	intN_to_str( n1, sres, 8 );
	printf( "nasobeni %s\n", sres );

	str_to_intN( sn1, n1, 8 );
	str_to_intN( sn2, n2, 8 );
	div_intN_intN( n1, n2, 8 );
	intN_to_str( n1, sres, 8 );
	printf( "deleni %s\n", sres );
	intN_to_str( n2, sres, 8 );
	printf( "deleni %s\n", sres );
	
}
