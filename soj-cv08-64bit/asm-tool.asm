	bits 64

	section .data

	section .text

;;;
;;; Examples
;;;

;;; ADD in 64b
	global add_intN_intN
; RDI, RSI, RDX, RCX, R8, R9
add_intN_intN:
	; enter 0,0
	; push ebx
	; push esi

	; mov edx, [ ebp + 8 ]        ; aN
	; mov esi, [ ebp + 12 ]       ; bN
	; mov ecx, [ ebp + 16 ]       ; N
	; mov ebx, 0                  ; inx
	; clc                         ; CF=0

	mov rcx, rdx	; size
	mov rdx, 0
	clc

.back:
	; mov eax, [ esi + ebx * 4 ]
	mov eax, [rsi + rdx * 4]	; rax = bN[edx]
	; adc [ edx + ebx * 4 ], eax  ; aN[inx]+=bN[inx]
	adc [rdi + rdx * 4], eax
	; inc ebx
	inc rdx
	loop .back

	ret

;;; sub
	global sub_intN_intN
; RDI, RSI, RDX, RCX, R8, R9
sub_intN_intN:
; 		mov edx, [ ebp + 8 ]        ; aN
; 		mov esi, [ ebp + 12 ]       ; bN
; 		mov ecx, [ ebp + 16 ]       ; N
; 		mov ebx, 0                  ; inx
; 		clc                         ; CF=0

	mov rcx, rdx
	mov rdx, 0
	clc

.back:
	mov eax, [rsi + rdx * 4]	; rax = bN[edx]
	sbb [rdi + rdx * 4], eax
	inc rdx
	loop .back

	ret


	global shift_left_intN
shift_left_intN:
	mov rcx, rsi
	mov rax, 0
	clc

.back:
	rcl dword [ rdi + rax * 4 ], 1
	inc rax
	loop .back

	mov rax, 0
	setc al
; 		;adc eax, 0
; 		setc al
; 		;adc byte [ edx ], 0 ; uprava na rotaci

	ret

	global shift_right_intN
shift_right_intN:
; 		mov ecx, [ ebp + 12 ];      ; size
; 		mov edx, [ ebp + 8 ]        ; array	
; 		clc

	mov rcx, rsi
	mov rax, 0
	clc

.back:
	rcr dword [ rdi + rcx * 4 - 4 ], 1
	loop .back

	mov rax, 0
	setc al
; 		mov eax, 0
; 		;adc eax, 0
; 		setc al

	ret

;;; 
; RDI, RSI, RDX, RCX, R8, R9
	global div_divN_int32
div_divN_int32:
	mov rcx, rdx	; size
	mov rdx, 0

.back:
	mov eax, [rdi + rcx * 4 - 4]
.test:
	div esi
.test2:
	mov [rdi + rcx * 4 - 4], eax
	loop .back

	mov eax, edx

	ret

;;;
; RDI, RSI, RDX, RCX, R8, R9
	global add_intN_int32
add_intN_int32:
	mov rcx, rdx
	mov eax, esi	; presun pricitaneho cisla do eax, aby se mohlo provest znamenkove prizpusobeni registru RDX
	cdq

	mov eax, [rdi]
	add eax, dword esi
	mov [rdi], eax

	dec rcx
	mov r10, 1

.back:
	jnc .done
	adc [rdi + r10 * 4], edx
	inc r10
	loop .back

.done:

	ret

;;;
; RDI, RSI, RDX, RCX, R8, R9
	global mul_intN_int32
mul_intN_int32:
	push rbx

	mov rcx, rdx
	mov ebx, 0
	mov r11, 0

.back:
	mov eax, [rdi + r11 * 4]
	mul dword esi
	add eax, ebx
	adc edx, 0
	mov ebx, edx

	mov [rdi + r11 * 4], eax

	inc r11
	loop .back

	pop rbx
	ret