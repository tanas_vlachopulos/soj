//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining two modules in C.
//
//***************************************************************************

#include <stdio.h>

// Variables

// public global variable
int tool_x_pub;

// local variable
static int tool_x_loc;

// external variable
extern int main_x_pub;

// Functions

// public function
int tool_fun_pub()
{
  main_x_pub = 40;
  //main_x_loc = 30; // impossible
  tool_x_pub = 20;
  tool_x_loc = 10;
}

// local function
static int tool_fun_loc()
{
  main_x_pub = 100;
  //main_x_loc = 200; //impossible
  tool_x_pub = 300;
  tool_x_loc = 400;
}

