
#include <stdio.h>

#define SIZE 5

long long mul_big(long long a, int b);
long long div_big(long long a, int b, int *zbytek);
int div_divN_int32(int *intN, int int32, int size);
int add_intN_int32(int *intN, int int32, int size);
int mul_intN_int32(int *intN, int int32, int size);

int is_zero(int *intN, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (intN[i] != 0)
			return 0;
	}
	return 1;
}

char *intN_to_str(int *intN, char *str, int size)
{
	while( !is_zero(intN, size))
	{
		int zbytek = div_divN_int32(intN, 10, size);
		*str = zbytek + '0';
		str++;
	}
	*str = 0;
}

char *str_to_intN(char *str, int *intN, int size)
{
	for (int i = 0; i < size; i++)
		intN[i] = 0;

	while( *str )
	{
		mul_intN_int32(intN, 10, size);
		add_intN_int32(intN, *str - '0', size);
		str++;
	}
}

void reverse_str(char *str)
{

}

int main()
{
	int int_160bit[SIZE] = {1, 0, 0, 0, 1};

	// test div N size array
	printf("zbytek %d \n", div_divN_int32(int_160bit, 3, 5));
	for (int i = 0; i < SIZE; i++)
	{
		printf("%08X ", int_160bit[SIZE - 1 - i]);
	}
	printf("\n");
	char str[60];
	intN_to_str(int_160bit, str, SIZE);
	printf("vysledek: %s\n", str);

	// test add N size array
	int int_160bit_1[SIZE] = {1, 0, 0, 0, 1};	
	add_intN_int32(int_160bit_1, 1, SIZE); // int_160bit + 1
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", int_160bit_1[SIZE - 1 - i]);
	printf("\n");

	//test mul N size array
	int int_160bit_2[SIZE] = {0, 0, 0, 0, 0xff};	
	mul_intN_int32(int_160bit_2, 2, SIZE); // int_160bit *= 2
	for (int i = 0; i < SIZE; i++)
		printf("%08X ", int_160bit_2[SIZE - 1 - i]);
	printf("\n");

	// char str[60] = "340282366920938463463374607431768211456";
	
	// printf("%Ld \n", mul_big(20000000001, 200));
	// int zbytek;
	// printf("zbytek: %d, result: %Ld \n", zbytek, div_big(20000000001, 2, &zbytek));
	
}
