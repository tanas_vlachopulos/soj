//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>
#include <string.h>

long long mul_i64_i32( long long a, int b );
long long div_i64_i32( long long a, int b, int *rem );
// intN = intN / int32
int div_intN_int32( int *intN, int int32, int N );
int add_intN_int32( int *intN, int int32, int N );
int mul_intN_int32( int *intN, int int32, int N );

int is_zero( int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        if ( intN[ i ] ) return 0;

    return 1;
}

char *intN_to_str( int *intN, char *str, int N )
{
    while ( !is_zero( intN, N ) )
    {
        int zbytek = div_intN_int32( intN, 10, N );
        *str = zbytek + '0';
        str++;
    }
    *str = 0;
}

char *str_to_intN( char *str, int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        intN[ i ] = 0;
    while ( *str )
    {
        mul_intN_int32( intN, 10, N );
        add_intN_int32( intN, *str - '0', N );
        str++;
    }
}

int main()
{
    int i160[ 5 ] = { 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 1 };
    char str[ 60 ] = "340282366920938463463374607431768211456";

    str_to_intN( str, i160, 5 );
    for ( int i = 0; i < 5; i++ )
        printf( "%08X ", i160[ 4 - i ] );
    printf( "\n" );
    return 0;
 
    intN_to_str( i160, str, 5 );
    printf( "vysledek je %s\n", str );

    return 0;
    int z = div_intN_int32( i160, 2, 5 );
    for ( int i = 0; i < 5; i++ )
        printf( "%08X ", i160[ 4 - i ] );
    printf( "zbytek %d\n", z );
}
