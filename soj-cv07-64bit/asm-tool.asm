	bits 64

	section .data

	section .text

;;;
;;; Examples
;;;

;;; multiple big numbers
	global mul_big
mul_big:
	; enter 0,0
	; push edi

	; mov eax, [ebp + 8]	; lower part of long long a
	; mul dword [ebp + 16] ; a_l *= big

	; mov ecx, eax
	; mov edi, edx

	; mov eax, [ebp + 12]		; hight part of long long big
	; mul dword [ebp + 16]	; a_h *= big

	; add edi, eax	; secte mezivysledek z prvniho nasobeni
	; mov edx, edi	; prepare return value
	; mov eax, ecx	; prepare reteurn value

	; pop edi
	; leave
	ret

;;; div big numbers
	global div_big
div_big:
	; enter 0,0
	; push edi

	; mov edx, 0		; prepare edx reg
	; mov eax, [ebp + 12]		;eax = a_h
	; div dword [ebp + 16]	; a_h /= b

	; mov ecx , eax			; save mezivysledek

	; mov eax, [ebp + 8]		; eax = a_l
	; div dword [ebp + 16]	; a_l /= b

	; ; return zbytek
	; mov edi, [ebp + 20]
	; mov [edi], edx			; zbytek = edx

	; mov edx, ecx			; prepare return value

	; pop edi
	; leave
	ret

;;; 
; RDI, RSI, RDX, RCX, R8, R9
	global div_divN_int32
div_divN_int32:
	mov rcx, rdx	; size
	mov rdx, 0

.back:
	mov eax, [rdi + rcx * 4 - 4]
.test:
	div esi
.test2:
	mov [rdi + rcx * 4 - 4], eax
	loop .back

	mov eax, edx

	ret

;;;
; RDI, RSI, RDX, RCX, R8, R9
	global add_intN_int32
add_intN_int32:
	mov rcx, rdx
	mov eax, esi	; presun pricitaneho cisla do eax, aby se mohlo provest znamenkove prizpusobeni registru RDX
	cdq

	mov eax, [rdi]
	add eax, dword esi
	mov [rdi], eax

	dec rcx
	mov r10, 1

.back:
	jnc .done
	adc [rdi + r10 * 4], edx
	inc r10
	loop .back

.done:

	ret

;;;
; RDI, RSI, RDX, RCX, R8, R9
	global mul_intN_int32
mul_intN_int32:
	push rbx

	mov rcx, rdx
	mov ebx, 0
	mov r11, 0

.back:
	mov eax, [rdi + r11 * 4]
	mul dword esi
	add eax, ebx
	adc edx, 0
	mov ebx, edx

	mov [rdi + r11 * 4], eax

	inc r11
	loop .back

	pop rbx
	ret
