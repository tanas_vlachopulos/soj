#include <strings.h>
#include <string.h>
#include <stdio.h>

#define K 14

int main(void)
{
	long long pi = 0;
	
	for (int k = 1; k <= K; k++)
	{
		long long e1 = 16;
		for (int i = 2; i <= k; i++)
		{
			e1 *= 16;
		}
		printf("e1 = %Ld\n", e1);

		e1 = 100 / e1;

		long long e2 = (400 / ((8 * k) + 1)) - (200 / ((8 * k) + 4)) - (100 / ((8 * k) + 5)) - (100 / ((8 * k) + 6));
		
		pi += e1 * e2;
	}
	printf("Pi: %Ld\n", pi);

	return 0;
}