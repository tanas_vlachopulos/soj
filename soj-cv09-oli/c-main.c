//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>

int shl_intN( int *iN, int N );
int shr_intN( int *iN, int N );
void shrd_intN( int *iN, int okolik, int N );
void shld_intN( int *iN, int okolik, int N );
void add_intN_intN( int *aN, int *bN, int N );
void sub_intN_intN( int *aN, int *bN, int N );

long long mul_i64_i32( long long a, int b );
long long div_i64_i32( long long a, int b, int *rem );
long long shrd_i64( long long a, int okolik );
long long shld_i64( long long a, int okolik );
// intN = intN / int32
int div_intN_int32( int *intN, int int32, int N );
int add_intN_int32( int *intN, int int32, int N );
int mul_intN_int32( int *intN, int int32, int N );

void str_mirror( char *str )
{
    int l = strlen( str );
    char tmp[ l + 1 ];
    for ( int i = 0; i < l; i++ )
        tmp[ l - i - 1 ] = str[ i ];
    tmp[ l ] = 0;
    strcpy( str, tmp );
}

int is_zero( int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        if ( intN[ i ] ) return 0;

    return 1;
}

// compare of two intN numbers
// a >  b return  1
// a == b return  0
// a <  b return -1
int cmp_intNintN( int *a, int *b, int N )
{
  int i;
  for ( i = N - 1; i >= 0; i-- )
  {
      if ( a[ i ] == b[ i ] ) continue;
      if ( ( unsigned int ) a[ i ] > ( unsigned int ) b[ i ] ) return 1;
      return -1;
  }
  return 0;
}

void mul_intN_intN( int *aN, int *bN, int N )
{
    int res[ N ];
    bzero( res, sizeof( res ) );

    for ( int i = 0; i < N * 32; i++ )
    {
        if ( bN[ 0 ] & 1 )
        {
            add_intN_intN( res, aN, N );
        }
        shl_intN( aN, N );
        shr_intN( bN, N );
    }

    memcpy( aN, res, sizeof( res ) );
}

void div_intN_intN( int *aN, int *bN, int N )
{
    int divadlo[ 2 * N ];
    int res[ N ];
    bzero( res, sizeof( res ) );
    bzero( divadlo, sizeof( divadlo ) );
    memcpy( divadlo, aN, N * sizeof( int ) );
    int *scena = divadlo + N;

    for ( int i = 0; i < N * 32; i++ )
    {
        shl_intN( divadlo, 2 * N );
        shl_intN( res, N ); 
        if ( cmp_intNintN( scena, bN, N ) >= 0 )
        {
            res[ 0 ] |= 1;
            sub_intN_intN( scena, bN, N );
        }
    }
    memcpy( aN, res, N * sizeof( int ) );
    memcpy( bN, scena, N * sizeof( int ) ); // zbytek
}

int div_int32_int32( int a, int b )
{
    long long delenec = a;  // divadlo int[ 2 ]
    int *scena = ( int * ) &delenec;
    scena++;                // &int[ 1 ]
    int res = 0;

    for ( int i = 0; i < 32; i++ )
    {
        res <<= 1;
        delenec <<= 1;
        if ( *scena >= b )
        {
            res |= 1;
            *scena -= b;
        }

    }
    return res;
}

int sqrt_int32( int a )
{
    long long divadlo = a;
    int *scena = ( int * ) &divadlo;
    scena++;
    int res = 0;

    for ( int i = 0; i < 16; i++ )
    {
        res <<= 1;
        divadlo <<= 2;
        int B = res * 2 * 1 + 1 * 1;
        if ( *scena >= B )
        {
            res |= 1;
            *scena -= B;
        }
    }
    return res;
}

char *intN_to_str( int *intN, char *str, int N )
{
    char *tmp = str;
    while ( !is_zero( intN, N ) )
    {
        int zbytek = div_intN_int32( intN, 10, N );
        *str = zbytek + '0';
        str++;
    }
    *str = 0;

    str_mirror( tmp );
}

char *str_to_intN( char *str, int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        intN[ i ] = 0;
    while ( *str )
    {
        mul_intN_int32( intN, 10, N );
        add_intN_int32( intN, *str - '0', N );
        str++;
    }
}

int main()
{
    int N = 1040;
    int Ns = 10010;
    int Z[ N ], Zc[ N ], fakt[ N ], faktc[ N ];
    char Zs[ Ns ];
    char vysls[ Ns ];
    int vysl[ N ];

    bzero( vysl, sizeof( vysl ) );
    memset( Zs, '0', sizeof( Zs ) ); // "0000.."
    Zs[ 10001 ] = 0;
    Zs[ 0 ] = '1';                   // "10000...\0"
    str_to_intN( Zs, Z, N );
    memcpy( vysl, Z, sizeof( Z ) ); // vysl = 1
    mul_intN_int32( vysl, 2, N );   // vysl = 2
    bzero( faktc, sizeof( faktc ) );
    faktc[ 0 ] = 1;                 // fakt=1
    
    for ( int i = 2; i < 1000; i++ )
    {
        printf( "i=%d\n", i );
        mul_intN_int32( faktc, i, N );
        memcpy( Zc, Z, sizeof( Z ) );
        memcpy( fakt, faktc, sizeof( fakt ) );
        div_intN_intN( Zc, fakt, N );
        add_intN_intN( vysl, Zc, N );
    }

    intN_to_str( vysl, vysls, N );
    printf( "e=%s\n", vysls );
}

int main_fakt()
{
    int cislo[ 270 ];
    char vysl[ 8600 ];

    bzero( cislo, sizeof( cislo ) );
    cislo[ 0 ] = 1;
    for ( int i = 2; i <= 1000; i++ )
        mul_intN_int32( cislo, i, 270 );

    intN_to_str( cislo, vysl, 270 );
    printf( "1000!=%s\n", vysl );
}

int main1()
{
    int cislo[ 4 ] = { 0xcafecafe, 0xbeefbeef, 0xcafecafe, 0xbeefbeef };
    printf( "odmocnina %d\n", sqrt_int32( 100000000 ) );
    printf( "shrd %Ld\n", shrd_i64( 320000000000, 4 ) );
    printf( "shld %Ld\n", shld_i64( 100000000000, 6 ) );

    shrd_intN( cislo, 16, 4 );
    for ( int i = 0; i < 4; i++ )
        printf( "%08X ", cislo[ 3 - i ] );
    printf( "\n" );
}


int main0()
{
    char sn1[ 100 ] = "123456789123456789123456789";
    char sn2[ 100 ] = "10000000000";
    char sres[ 100 ];

    int n1[ 8 ], n2[ 8 ];
    str_to_intN( sn1, n1, 8 );
    str_to_intN( sn2, n2, 8 );

    mul_intN_intN( n1, n2, 8 );
    intN_to_str( n1, sres, 8 );
    printf( "nasobeni %s\n", sres );

    str_to_intN( sn1, n1, 8 );
    str_to_intN( sn2, n2, 8 );
    div_intN_intN( n1, n2, 8 );
    intN_to_str( n1, sres, 8 );
    printf( "deleni %s\n", sres );
    intN_to_str( n2, sres, 8 );
    printf( "deleni %s\n", sres );

}

int mmain()
{
    int n1[ 8 ] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
    int n2[ 8 ] = { 0x10000, 0, 0, 0, 0, 0, 0, 0 };
    //add_intN_intN( n1, n2, 8 );

    printf( "deleni %d\n", div_int32_int32( 10, 3 ) );

    div_intN_intN( n1, n2, 8 );

    for ( int i = 0; i < 8; i++ )
        printf( "%08X ", n1[ 7 - i ] );
    printf( "\n" );
    for ( int i = 0; i < 8; i++ )
        printf( "%08X ", n2[ 7 - i ] );
    printf( "\n" );
}
