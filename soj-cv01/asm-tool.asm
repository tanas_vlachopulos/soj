;***************************************************************************
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 32

	section .data

; external variable
extern  main_x_pub
extern  main_x_loc
extern  my_variable_int
extern	my_var_char
extern my_int_arr
extern my_char_arr
extern index
extern box
extern edit_arr

; public variable
global  tool_x_pub
global	my_asm_var_int

; local variables
tool_x_pub      dd      0
tool_x_loc      dd      0
my_asm_var_int	dd	54
local_int_arr	dd 	10, 11, 12, 13, 14

        section .text

; public label (function)
global  tool_fun_pub

tool_fun_pub:
	enter 0,0

        mov     [ main_x_pub ], dword 40
        ;mov     [ main_x_loc ], dword 30     ; impossible
        mov     [ tool_x_pub ], dword 20
        mov     [ tool_x_loc ], dword 10
	mov	[ my_variable_int], dword 43 	; edit my variable
	mov	[ my_var_char ], dword 'u'	; edit char var

	mov	edx, 5
	mov	[ my_int_arr + edx * 4], dword 10
	mov	edx, 6
	mov	[ my_int_arr + edx * 4], dword 11
	mov	edx, 7
	mov	[ my_int_arr + edx * 4], dword 12
	mov	edx, 8
	mov	[ my_int_arr + edx * 4], dword 13
	mov	edx, 9
	mov	[ my_int_arr + edx * 4], dword 14

	mov	[ my_char_arr + 9 * 1 ], byte 'M'
	mov	[ my_char_arr + 8 * 1 ], byte 'Q'

	mov	eax, [ index ]
	mov	ebx, [ edit_arr + eax * 4 ]
 	mov	[ box ], ebx

        leave
        ret

tool_fun_loc:
	enter 0,0

        mov     [ main_x_pub ], dword 100
        ;mov     [ main_x_loc ], dword 200     ; impossible
        mov     [ tool_x_pub ], dword 300
        mov     [ tool_x_loc ], dword 400
	

        leave
        ret
