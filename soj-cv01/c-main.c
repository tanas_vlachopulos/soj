//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>

// Variables

// public global variable
int main_x_pub;
int my_variable_int;
char my_var_char;
int my_int_arr[10];
char my_char_arr[10];
int edit_arr[10];
int index = 5;
int box = 0;

// local variable
static int main_x_loc;

// external variable
extern int tool_x_pub;
extern int my_asm_var_int;

// Functions

// public function
int main_fun_pub()
{
  main_x_pub = -3;
  main_x_loc = -2;
  tool_x_pub = -1;
  my_asm_var_int = 55;

  for (int i = 0; i < 5; i++) {
	my_int_arr[i] = i;
	my_char_arr[i] = (char)(i + 65);
	edit_arr[i] = i;
	edit_arr[i+5] = i+5;
  }
  //tool_x_loc = 0; // impossible
}

// local function
static int main_fun_loc()
{
  main_x_pub = 1;
  main_x_loc = 2;
  tool_x_pub = 3;
  my_variable_int = 42;
  my_var_char = 't';


  //tool_x_loc = 0; // impossible
}

// external function
extern int tool_fun_pub();
extern int tool_fun_loc();


// print function
int print()
{
  printf( "main_x_pub=%d main_x_loc=%d tool_x_pub=%d\n", main_x_pub, main_x_loc, tool_x_pub );
  printf("My variables int_c=%d char=%c box=%d \n", my_variable_int, my_var_char, box);

  for (int i = 0; i < 10; i++) {
	printf("%d ", my_int_arr[i]);
  }
  printf("\n");
  for (int i = 0; i < 10; i++) {
	printf("%c ", my_char_arr[i]);
  }
  printf("\n");
  for (int i = 0; i < 10; i++) {
	printf("%d ", edit_arr[i]);
  }
  printf("\n");
}

int main()
{
  main_fun_loc();
  print();
  main_fun_pub();
  print();
  tool_fun_pub();
  print();
  //tool_fun_loc(); // impossible
  //print();
}
