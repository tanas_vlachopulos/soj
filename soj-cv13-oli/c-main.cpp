//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>
#include <string.h>
#include <stdint.h>

//extern "C" {

double retdouble( double x );
float retfloat( float x );
double retdouble2( float x );
float retfloat2( double x );
float int2float( int a );

double sum( double a, double b );
float sum_array( float *ptr, int N );
int pocet_zapornych( float *ptr, int N );

//}

int main()
{
    float pole[ 12 ] = { 1, 2, -3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    printf( "suma pole %f\n", sum_array( pole, 12 ) );
    printf( "pocet zapornych %d\n", pocet_zapornych( pole, 12 ) );

}

int mmain()
{
    printf( "sum %f\n", sum( 3333.333, 4444.444 ) );
    printf( "double %f\n", retdouble( 3.14 ) );
    printf( "float %f\n", retfloat( 10.0 / 3 ) );
    printf( "double %f\n", retdouble2( 3.14 ) );
    printf( "float %f\n", retfloat2( 10.0 / 3 ) );
    printf( "int2float %f\n", int2float( 333 ) );
}
