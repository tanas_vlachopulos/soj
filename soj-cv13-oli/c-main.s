	.file	"c-main.cpp"
	.section	.rodata
.LC12:
	.string	"suma pole %f\n"
.LC13:
	.string	"pocet zapornych %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movss	.LC0(%rip), %xmm0
	movss	%xmm0, -64(%rbp)
	movss	.LC1(%rip), %xmm0
	movss	%xmm0, -60(%rbp)
	movss	.LC2(%rip), %xmm0
	movss	%xmm0, -56(%rbp)
	movss	.LC3(%rip), %xmm0
	movss	%xmm0, -52(%rbp)
	movss	.LC4(%rip), %xmm0
	movss	%xmm0, -48(%rbp)
	movss	.LC5(%rip), %xmm0
	movss	%xmm0, -44(%rbp)
	movss	.LC6(%rip), %xmm0
	movss	%xmm0, -40(%rbp)
	movss	.LC7(%rip), %xmm0
	movss	%xmm0, -36(%rbp)
	movss	.LC8(%rip), %xmm0
	movss	%xmm0, -32(%rbp)
	movss	.LC9(%rip), %xmm0
	movss	%xmm0, -28(%rbp)
	movss	.LC10(%rip), %xmm0
	movss	%xmm0, -24(%rbp)
	movss	.LC11(%rip), %xmm0
	movss	%xmm0, -20(%rbp)
	leaq	-64(%rbp), %rax
	movl	$12, %esi
	movq	%rax, %rdi
	call	_Z9sum_arrayPfi
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC12, %edi
	movl	$1, %eax
	call	printf
	leaq	-64(%rbp), %rax
	movl	$12, %esi
	movq	%rax, %rdi
	call	_Z15pocet_zapornychPfi
	movl	%eax, %esi
	movl	$.LC13, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L3
	call	__stack_chk_fail
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
.LC16:
	.string	"sum %f\n"
.LC18:
	.string	"double %f\n"
.LC20:
	.string	"float %f\n"
.LC23:
	.string	"int2float %f\n"
	.text
	.globl	_Z5mmainv
	.type	_Z5mmainv, @function
_Z5mmainv:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movsd	.LC14(%rip), %xmm0
	movabsq	$4659548491859221283, %rax
	movapd	%xmm0, %xmm1
	movq	%rax, -8(%rbp)
	movsd	-8(%rbp), %xmm0
	call	_Z3sumdd
	movl	$.LC16, %edi
	movl	$1, %eax
	call	printf
	movabsq	$4614253070214989087, %rax
	movq	%rax, -8(%rbp)
	movsd	-8(%rbp), %xmm0
	call	_Z9retdoubled
	movl	$.LC18, %edi
	movl	$1, %eax
	call	printf
	movss	.LC19(%rip), %xmm0
	call	_Z8retfloatf
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC20, %edi
	movl	$1, %eax
	call	printf
	movss	.LC21(%rip), %xmm0
	call	_Z10retdouble2f
	movl	$.LC18, %edi
	movl	$1, %eax
	call	printf
	movabsq	$4614688418178968235, %rax
	movq	%rax, -8(%rbp)
	movsd	-8(%rbp), %xmm0
	call	_Z9retfloat2d
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC20, %edi
	movl	$1, %eax
	call	printf
	movl	$333, %edi
	call	_Z9int2floati
	cvtss2sd	%xmm0, %xmm0
	movl	$.LC23, %edi
	movl	$1, %eax
	call	printf
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	_Z5mmainv, .-_Z5mmainv
	.section	.rodata
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	1073741824
	.align 4
.LC2:
	.long	3225419776
	.align 4
.LC3:
	.long	1082130432
	.align 4
.LC4:
	.long	1084227584
	.align 4
.LC5:
	.long	1086324736
	.align 4
.LC6:
	.long	1088421888
	.align 4
.LC7:
	.long	1090519040
	.align 4
.LC8:
	.long	1091567616
	.align 4
.LC9:
	.long	1092616192
	.align 4
.LC10:
	.long	1093664768
	.align 4
.LC11:
	.long	1094713344
	.align 8
.LC14:
	.long	2851858285
	.long	1085365361
	.align 4
.LC19:
	.long	1079334229
	.align 4
.LC21:
	.long	1078523331
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
