#include <stdio.h>

class test
{
  public:
    void tisk() { printf( "prom: %d\n", this->prom ); }

    int prom;
} __attribute__((packed));

class test2 : public test
{
  public:
    virtual void tisk() { printf( "novy format %d\n", prom ); }
};

int main()
{
    test a;
    test2 b;
    printf( "size %ld %ld\n", sizeof( a ), sizeof( b ) );
    a.prom = 33;
    b.prom = 44;
    test *p1 = &a;
    test2 *p2 = &b;
    p1->tisk();
    p2->tisk();
}
