	.file	"testik.cpp"
	.section	.rodata
.LC0:
	.string	"prom: %d\n"
	.section	.text._ZN4test4tiskEv,"axG",@progbits,_ZN4test4tiskEv,comdat
	.align 2
	.weak	_ZN4test4tiskEv
	.type	_ZN4test4tiskEv, @function
_ZN4test4tiskEv:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	_ZN4test4tiskEv, .-_ZN4test4tiskEv
	.section	.rodata
.LC1:
	.string	"novy format %d\n"
	.section	.text._ZN5test24tiskEv,"axG",@progbits,_ZN5test24tiskEv,comdat
	.align 2
	.weak	_ZN5test24tiskEv
	.type	_ZN5test24tiskEv, @function
_ZN5test24tiskEv:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	_ZN5test24tiskEv, .-_ZN5test24tiskEv
	.section	.text._ZN4testC2Ev,"axG",@progbits,_ZN4testC5Ev,comdat
	.align 2
	.weak	_ZN4testC2Ev
	.type	_ZN4testC2Ev, @function
_ZN4testC2Ev:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	_ZN4testC2Ev, .-_ZN4testC2Ev
	.weak	_ZN4testC1Ev
	.set	_ZN4testC1Ev,_ZN4testC2Ev
	.section	.text._ZN5test2C2Ev,"axG",@progbits,_ZN5test2C5Ev,comdat
	.align 2
	.weak	_ZN5test2C2Ev
	.type	_ZN5test2C2Ev, @function
_ZN5test2C2Ev:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZN4testC2Ev
	movl	$_ZTV5test2+16, %edx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	_ZN5test2C2Ev, .-_ZN5test2C2Ev
	.weak	_ZN5test2C1Ev
	.set	_ZN5test2C1Ev,_ZN5test2C2Ev
	.section	.rodata
.LC2:
	.string	"size %ld %ld\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN5test2C1Ev
	movl	$16, %edx
	movl	$4, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movl	$33, -64(%rbp)
	movl	$44, -24(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -48(%rbp)
	leaq	-32(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN4test4tiskEv
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, %rdi
	call	*%rax
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L7
	call	__stack_chk_fail
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.weak	_ZTV5test2
	.section	.rodata._ZTV5test2,"aG",@progbits,_ZTV5test2,comdat
	.align 8
	.type	_ZTV5test2, @object
	.size	_ZTV5test2, 24
_ZTV5test2:
	.quad	0
	.quad	_ZTI5test2
	.quad	_ZN5test24tiskEv
	.weak	_ZTI5test2
	.section	.rodata._ZTI5test2,"aG",@progbits,_ZTI5test2,comdat
	.align 8
	.type	_ZTI5test2, @object
	.size	_ZTI5test2, 40
_ZTI5test2:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS5test2
	.long	0
	.long	1
	.quad	_ZTI4test
	.quad	2050
	.weak	_ZTS5test2
	.section	.rodata._ZTS5test2,"aG",@progbits,_ZTS5test2,comdat
	.type	_ZTS5test2, @object
	.size	_ZTS5test2, 7
_ZTS5test2:
	.string	"5test2"
	.weak	_ZTI4test
	.section	.rodata._ZTI4test,"aG",@progbits,_ZTI4test,comdat
	.align 8
	.type	_ZTI4test, @object
	.size	_ZTI4test, 16
_ZTI4test:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS4test
	.weak	_ZTS4test
	.section	.rodata._ZTS4test,"aG",@progbits,_ZTS4test,comdat
	.type	_ZTS4test, @object
	.size	_ZTS4test, 6
_ZTS4test:
	.string	"4test"
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
