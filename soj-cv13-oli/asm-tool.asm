;***************************************************************************
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 64

	section .data

        section .text
; rdi, rsi, rdx, rcx, r8 a r9 / rax, r10, r11

        global pocet_zapornych
pocet_zapornych:
        mov eax, 0              ; 0
        cvtsi2ss xmm0, eax      ; 0.0
        movsx rcx, esi
.back:
        comiss xmm0, [ rdi + rcx * 4 - 4 ]
        cmc
        adc eax, 0
.taknic:
        loop .back
        ret

        global sum_array
sum_array:
        enter 16, 0
        xorps xmm0,xmm0         ; sum = 0
        movsx rcx, esi          ; N
        shr rcx, 2              ; N /= 4
.back:
        addps xmm0, [ rdi ]
        add rdi, 16
        loop .back

        movaps [ rbp - 16 ], xmm0
        addss xmm0, [ rbp - 12 ]
        addss xmm0, [ rbp - 8 ]
        addss xmm0, [ rbp - 4 ]
        leave

        ret

        global sum
sum:
        addsd xmm0, xmm1
        ret

        global int2float
int2float:
        cvtsi2ss xmm0, edi
        ret

        global retdouble
retdouble:
        ret

        global retfloat
retfloat:
        ret

        global retdouble2
retdouble2:
        cvtss2sd xmm0, xmm0
        ret

        global retfloat2
retfloat2:
        cvtsd2ss xmm0, xmm0
        ret

