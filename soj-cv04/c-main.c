/*******
1. upravit str2int na base 8/10/16 dle prvniho znaku:
  0/cislice/X
  077=63
  123=123
  XFF=255

2. Napiste "int sum( char a, char b )"

3. Otocit poradi prvku v poli, posledni cislo je -1.

4. Odstraneni cislic z retezce. 
*************/
#include <stdio.h>

#define array_size 9

void to_uppercase_cmov();

int sum (int a, int b);
int mult(int a, int b);
long long mult64(int a, int b); // 64bit signed integer (long long)
int num_positive(int *array, int size);
int num_positive_setcc(int *array, int size);
void sum_vector(int *v, int *u, int size);
void search_max(int *array, int size, int *max); 
void str_len(char *str);
int str_to_int(const char *str);
int find_zero(int *array);


// tasks
int char_sum(char a, char b);
char* remove_numbers_from_str(char *str);
int str_to_number(char *str);
void switch_array(int *array, int size);

int debug;

int main()
{
	int max[1];
	int arr[5] = {-1, 2, 6, -5, 5};
	int arr2[5] = {-1, 0, 6, -5, 5};	

	printf("Result of sum: %d\n", sum(-10, 20));
	printf("Mult: %d\n", mult(1000000, 20000000));
	printf("Mult64: %Ld\n", mult64(1000000, 20000000));
	printf("positive number in array: %d\n", num_positive(arr, 5));
	printf("positive couter setcc: %d\n", num_positive_setcc(arr, 5));
	printf("cislo z retezce je %d\n", str_to_int("123"));

	printf("find zero %d \n", find_zero(arr2));

	/*****
	TASKS
	*****/

	// Task 4
	char str1[] = "a1ho5j";
	remove_numbers_from_str(str1);
	printf("String: %s\n", str1);

	// Task 2
	printf("Char sum: %d\n", char_sum(-128, 127));

	// Task 1
	printf("Char to number: %d\n", str_to_number("123"));	
	printf("Char to number: %d\n", str_to_number("0110"));
	printf("Char to number: %d\n", str_to_number("X66"));
	printf("Char to number: %d\n", str_to_number("XFF"));
	printf("Char to number: %d\n", str_to_number("XAA"));
	
	// Task 3
	int arr3[array_size] = {1, 2, 3, 4, 5, 6, -1, 0, 8};
	switch_array(arr3, array_size);
	for (int i = 0; i < array_size; i++) {
		printf("%d  ", arr3[i]);
	}
	printf("\n");
}
