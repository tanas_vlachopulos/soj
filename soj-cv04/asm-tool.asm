	bits 32

	section .data

	section .text
	global sum
	global mult, mult64
	global num_positive
	global num_positive_pointer
	global num_positive_setcc
	global sum_vector
	global search_max
	global str_len
	global find_zero
	global str_to_int
	global char_sum
	global remove_numbers_from_str
	global str_to_number
	global switch_array

	extern debug

sum:
	enter 0,0				; zacatek funkce bez lokalnich promennych a zanoreni
	;
	mov eax, [ ebp + 8 ]	; prvni argument je vzdy na ebp + 8 prvni 2 pozici jsou urceny pro hodnotu EBP a ukazatel na zacatek funkce
	add eax, [ ebp + 12 ]	; argument jsou vzdy ulozeny v prostoru o velikosti 4 byte
	;
	leave
	ret

; v asm lze definovat funkci s 2 vstupnimi body (v C nelze)
; funkce imul ulozi vysledek  do EAX a EDX
; v zavislosi na tom jaky navratovy typ nadefinujeme v C se bude vracet bud registr EAX, nebo oba registry EAX a EDX
; aby bylo mozno predat 64 bitove cislo musi se predat dva registry EAX i EDX
mult:
mult64:
	enter 0,0
	;
	mov eax, [ebp + 8]
	imul dword [ebp + 12]	; velikost dword musi byt specifikovana
	;
	leave
	ret

num_positive:
	enter 0,0
	;
	mov ecx, [ebp + 12]			; nacte velikost pole do citaciho registru
	mov edx, [ebp + 4]			; nacte ukazatel na pole

	mov eax, 0 					; clear eax
.back:
	cmp dword [edx + ecx * 4 - 4], 0		; pristup k prvkum v poli, v EDX je ulozen ukazate na zacatek funkce
	jle .negative				; if number[ecx] > 0
	inc eax
.negative:
	loop .back
	;
	leave
	ret

num_positive_pointer:
	enter 0,0
	;
	mov ecx, [ebp + 12]			; nacte velikost pole do citaciho registru
	mov edx, [ebp + 4]			; nacte ukazatel na pole

	mov eax, 0 					; clear eax
.back:
	; cmp dword [edx + ecx * 4 - 4], 0		; pristup k prvkum v poli, v EDX je ulozen ukazate na zacatek funkce
	cmp dword [edx], 0
	jle .negative				; if number[ecx] > 0
	inc eax
.negative:
	add edx, 4
	loop .back
	;
	leave
	ret

num_positive_setcc:
	enter 0,0
	push ebx
	;
	mov ecx, [ebp + 12]			; nacte velikost pole do citaciho registru
	mov edx, [ebp + 4]			; nacte ukazatel na pole

	mov eax, 0 					; clear eax
.back:
	; cmp dword [edx + ecx * 4 - 4], 0		; pristup k prvkum v poli, v EDX je ulozen ukazate na zacatek funkce
	cmp dword [edx], 0
	setg bl
	add eax, ebx

	add edx, 4
	loop .back
	;
	pop ebx
	leave
	ret

sum_vector:
	enter 0,0
	push edi
	;
	mov ecx, [ebp + 16]		; load size 
	mov edx, [ebp + 12]		; load U vector
	mov edi, [ebp + 8]		; load V vector

.back:
	mov eax, [edx + ecx * 4 + 4]	; eax = U[ecx]
	add [edi + ecx * 4 - 4], eax	; V[ecx] += eax

	loop .back
	;
	pop edi
	leave
	ret

search_max:
	enter 0,0
	;
	mov ecx, [ebx + 12]		; load size
	mov edx, [ebp + 8]		; array
	mov eax, edx			; max = array[0]

.back:
	cmp eax, [edx + ecx * 4 + 4]
	jge .skip
	mov eax, [edx + ecx * 4 + 4]
.skip:
	loop .back

	mov ecx, [ebp + 16]		; load result pointer
	mov [ecx], eax			; *max = eax

	;
	leave
	ret

str_len:
	enter 0,0

	mov eax, [ebp + 8]
.back:
	cmp [eax], byte 0		; je dulezite mit byte, jinak by se porovnaly 4 byty, char ma jenom 1 byte
	je .done
	inc eax
	jmp .back

.done:


	leave
	ret

;;;
str_to_int:
	enter 0,0
	push ebx

	mov eax, 0				; N
	mov ebx, [ebp + 8]		; string
	mov ecx, 10				; zaklad desitkove soustavy
	
.back:
	cmp [ebx], byte 0
	je .done

	imul ecx
	mov edx, 0
	mov dl, [ebx]
	sub dl, '0'
	add eax, edx

	inc ebx	
	jmp .back

.done:
	pop ebx
	leave
	ret


find_zero:
	enter 0,0

	mov edx, [ebp + 8]
	mov eax, dword (-1)

.loop_lable:
	inc eax
	mov ecx, [edx + eax * 4]
	cmp ecx, dword 0
	jne .loop_lable

	leave
	ret

;;;
;;; TASKS
;;;

;;; Task 1
;;; conver string to number with 8, 10 and 16 base
str_to_number:
	enter 0,0
	push ebx

	mov eax, 0
	mov ebx, [ebp + 8]
	mov ecx, 10
	
	mov dl, [ebx]
	cmp dl, byte '0'
	je .octet
	cmp dl, byte 'X'
	je .hex

.loop_lable:
	cmp [ebx], byte 0
	je .done

	imul ecx
	mov edx, 0

	mov dl, [ebx]
	cmp dl, byte 'A'
	jae .alfabet_sub
	sub dl, '0'
	jmp .end_sub
.alfabet_sub:
	sub dl, byte 55
.end_sub:

	add eax, edx

	inc ebx	
	jmp .loop_lable

.octet:
	mov ecx, 8
	inc ebx
	jmp .loop_lable 

.hex:
	mov ecx, 16
	inc ebx
	jmp .loop_lable

.done:
	pop ebx
	leave
	ret

;;; Task 2
;;; return sum of 2 chars
char_sum:
	enter 0,0

	movsx eax, byte [ebp + 8]
	movsx edx, byte [ebp + 12]
	add eax, edx

	leave
	ret

;;; Task 3
;;; switch array between start and -1 item
switch_array:
	enter 0, 0
	push ebx
	push esi

	mov edx, [ebp + 8]		; *array
	mov ecx, dword [ebp + 12]		; array_length

	mov ebx, dword 0
.count_loop:
	mov eax, dword [edx + (ecx - 1) * 4]
	cmp eax, (-1)
	cmove ebx, ecx
	loop .count_loop

	sub ebx, 2
	cmp ebx, 1
	jl .invalid

	mov [debug], ebx


	mov eax, ebx			; eax = array_length
	mov ecx, dword 2		; ecx = 2
	cdq
	idiv ecx				; eax = array_length / 2
	mov ecx, eax			; ecx = half_array_length

	mov edx, [ebp + 8]		; edx = *array
.reverse_loop:
	mov esi, ebx
	sub esi, ecx
	mov eax, [edx + ecx * 4]	; tmp = array[ecx]
	xchg eax, [edx + esi * 4]
	mov [edx + ecx * 4], eax
	dec ecx
	cmp ecx, dword (-1)
	jne .reverse_loop

.invalid:
	pop ebx
	pop esi
	leave
	ret

;;; Task 4
;;; remove numbers from string
remove_numbers_from_str:
	enter 0,0
	push ebx
	;
	mov ecx, 0						; counter
	mov edx, [ebp + 8]		; string
	xor ax, ax

.loop_lable:
	mov al, byte [edx]
	
	mov bx, ' '

	cmp al, '0'
	cmovb bx, ax
	cmp al, '9'
	cmova bx, ax
	
	mov [edx], bl

	inc edx
	cmp al, byte 0
	jne .loop_lable

	mov eax, edx
	;
	pop ebx
	leave
	ret