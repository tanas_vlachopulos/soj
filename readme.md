SOJ
=========
CheatSheet
----------------

**Obecný postup tvorby programu:**

1. v C vytvořit potřebné proměnné
2. v ASM tyto proměnné definovat pomocí `extern` v bloku .data
3. v C vytvořit prototyp funkce
4. v ASM definovat tuto funkci a označit ji jako `global` na začátku bloku .text
5. vytvořit návěští pro funkci a ukončit ji pomocí `ret`
6. v C tuto funkci zavolat

**Kopírování na Linedu** (musí se přednastavit nologin ssh klient)

```
rsync --rsh="ssh" --verbose -r soj-cv03/ vsb:/home/fei/vla0054/soj-cv03
```

----------------------
Cv 01
----------------
> Pozn: Zdrojové kódy http://poli.cs.vsb.cz/edu/soj/src/
> 
> Pozn: aktuální zdrojové kódy jsou v adresáři na lindedu **home/fei/oli10/soj**

### Registry x86 procesorů
![enter image description here](https://lh3.googleusercontent.com/WV2IUdjwzK4xROglJXW7dNlDtp7KnMD77bp-NdXN2AN-4aWnUVC1Atfs_6f6vrIwQJGoVH5HOQ=s0 "pamet.png")

* Uživatelsky dostupných 8  32-bitových registrů
* Obvykle je používaných pouze  jejich spodní 16-bitová část
* Je nutné si dát pozor na modifikace těchto registrů, pokud modifikujeme horní část registrů můžeme znehodonotit obsah spodní části registrů
* Registry CS, DS, … GS nejsou určeny pro uživatele, systém je používá při běhu programů
* Flagy - detekují různé stavy při běhu programu
	* CF a OF - Carry a Overflow flag - detekují přetečení číselného registru
	* DF - Direction flag - používá se u operecích se stringy
	* SF
	* ZF - Zero flag - detekce nuly
* AX registr - akumulátor
* CX - čítač
* CL - čítač bitového posunu

### Přístup do paměti
* Přístup v 16 bitovém režimu
 ![enter image description here](https://lh3.googleusercontent.com/Ef0-VcgC6rH7QKICRqm44so1Nt2nFQWKr2YFifuC-3Uf6LJ_s7gsXDPe9E89cErEyAQZAlmDbw=s0 "pristupdopameti1.png")
* Přístup v 32 bit režimu
![enter image description here](https://lh3.googleusercontent.com/FCX3IVM5hb6KfdpUx388wS1EB7m7mvLuzsKWKC__gV-wXs86Ted6dBJqzLbO2F_CHiaBpUXz5g=s0 "pristupdopameti2.png")
* Pokud používáme jednu proměnou stačí přičíst konstantu, indexový registr se používá pokud máme v paměti uložené pole
* Uvnitř hranatých závorek se nesmí používat mínus, v případě že chceme odečítat, musíme přičíst negativní hodnotu


### Proměnné
* K dispozici je 5 datových typů, jediný rozdíl mezi nimi je velikost, překladač nijak neřeší typovost dat, to si řeší programátor sám
	
![enter image description here](https://lh3.googleusercontent.com/vaOoZ2Zfm2Stfim5oDgVkkAkJT85AjJ80L-V6kH8X9BsirUmQgV7VENe-H-FarKQA6F2oucSDg=s0 "datovetypy.png")
	
```
MOV R, R ; přesun registru do registru
MOV R, M ; přesun paměti do registru
MOV M, R ; přesun registru do paměti
MOV R, K ; přesun konstanty do registru
MOV M, K ; přesun konstanty do paměti
```


### Linkování
**V jazyce C**

* Pokud definujeme funkci nebo proměnou jako static budou tyto funkce dostupné pouze v kontextu daného modulu
* Automaticky je vše bez slova static veřejné a přístupné všem ostatním modulům
* Pokud chceme k nějaké funkci kt. Se nachází mimo aktuální modul musíme ji deklarovat s klíčovým slovem exteren
* U funkcí  není slovo extern poviné, překladač obvykle pozná že je externí, ale je to doporučováno používat

**V ASM**

* Všechny proměnné a funkce jsou defaultně privátní, abychom je učinili globální musíme  použít slovo global
* Na rozdíl od C nemůžeme proměnné definovat kde se nám zachce, proměnné lze definovat pouze v sekci .data
* Pokud vložíme proměnnou do hranatých závorek, přeloží se proměnná na adresu v paměti [ promenna ]

**Kompilace ASM a C programů**

* Přeložení ASM zdrojového souboru na object file
```
nasm -f elf32 asm-tool.asm
```
* S se vzniklým object souborem lze provést běžnou kompilaci s překladečem gcc
```
gcc -m32 c-main.c asm-tool.o -o cmain
```

-------------------------
Cv 02
------------------
nikdy nelze přesouvat mezi 2 adresami paměti, vždy musí zdroj nebo cíl registr

### Instrukce
**CMOVcc dest, src**
Podmíněné přesunutí. Pokud je splněna podmínka cc je proven přesun dest -> src.
Lze přesouvat jen 16 a 32 bitová čísla.
Nelze použít pro přesun z proměnné do paměti lze přiřadit pouze něco -> registr.

```
cmp eax, ebx		
cmovl ebx, eax		; if (eax < ebx) then {ebx = eax}
```
**MOVZ**
pro přiřazení menší proměnné do větší, např 8b do 32b
prázdné místo se vyplní 0
problém nastává pokud přesouváme proměnnou se znaménkem
např. -1 v 8bitovém formátu je `1111 1111`, ale pro přesunu do 32b proměnné se z -1 stane 255, což je špatně, musí se proto použít **MOVSX** kt. vyplní prázdné pozice znaménkovým rozlišením

**XCHC**
přehodí obsah dvou proměnných, funguje to ale jen mezi registry

**BSWAP**
zrcadlově obrátí obsah proměnné po **bytech** (ne po bitech), používá se jako konverze mezi little a big endianem

**IN, OUT**
přístup k externím portům

### Aritmetické operace

**INC reg**
Inkrementuje o 1 hodnotu v registru.

**ADC**
sečte s Carry flagem, cíl = cíl + zdroj + CF
Carry flag se nastaví, pokud dojde k přetečení proměnné - používá jako přenost při aritmetickách operacích s velkými čísly

**INC, DEC**
Přičte / odečte 1
nenastavuje CF, přetečení se detekuje jenom pomocí zero flagu
Tzn. pokud přičítáme a najednou se neám v registru objeví nula (nastaví se zero flag) víme že došlo k přetečení

**NEG**
vypočte zápornou hodnotu čísla, tzn provede
`NEG x = NOT (x - 1)` nebo také
`NET x = NOT (x) + 1`

**MUL src**
násobení
má jenom jeden argument, druhý argument musí být předpřipravený v akumulátoru
podle toho zda chceme násobit 8, 16, nebo 32 bitové číslo musíme předpřipravit AL, AX, nebo EAX
protože výsledek násobení dvou 16 bitových čísle přeteče velikost akumulátoru EAX, uloží se zbytek výpočtu do registru DX
výsledek součinu dvou 8b čísel se vejde do AX
součin dvou 16b čísel bude v AX a zbylá část v DX
součin dvou 32b bude v EAX a zbytek v EDX

**IMUL src**
podobné jako MUL, ale pro záporné číslo

**DIV src**
dělené číslo musí být předpřipraveno v akumulátoru, podle velikosti čísla v AX, AX-DX (horní část-dolní část), nebo EAX-EDX (horní část-dolní část)
instrukce obsah těchto registrů vidělí argumetem a výsledek uloží do registrů AL, AX, EAX a zbytek po dělení do AH, DX, EDX

**IDIV src**
znaménkové dělení
před provedením dělení je nutné přizpůsobit akumulátory pro záporné číslo, k tomu slouží **CDQ** 
funguje jako MOVSX akorát nad akumulátory `MOVSX AX, AL`

**CBW, CWD**
stejné jako CDQ, ale pro 8 a 16 bitové čísla


### Cykly
**LOOP dest**
pracuje s ECX registrem
```
if (--ECX > 0) {
	goto label;
}
```

**JMP dest**
Skočí na dané návěští.

**lokální návěští**
lokální návěští se mohou v programu opakovat, platí pouze mezi dvěma globálními návěštími
nelze s nimi dělat zanořené cykly
```
@tmp_label:				; lokalni navesti	
	; indexovani pole
	; od ecx se odecita 1, protoze v C jsou pole indexovane od nuly, ale v ASM skonci cyklus na indexu 1, 0 cyklus ukoncuje
	add eax, [ array + (ecx - 1) * 4 ]
	loop @tmp_label
	mov [sum], eax		; ulozi vysledek do promenne sum
```

### Větvení

**CMP dest, src**
Porovná registr dest a src, nebo registr dest a proměnou src.
provede odčítání, ale výsledek nevrátí do proměnné, ale nastaví jenom Cary flag

**Operátory:**
**!!!** Není možné porovávat znaménkové a neznaménkové proměnné (unsigned a signed)

C | ASM signed | ASM unsigned
--|------------|--------------
== | JE | JE
!= | JNE | JNE
< | JL/JNGE | JB/JNAE
> | JG/JNLE | JA/JNBE
\>= | JGE/JNL | JNB/JAE
<= | JLE/JNG | JNA/JBE

Písmena **A** (above) a **B** (below) slouží pro porovnávání bez znaménkových proměnných.
Písmena **L** (less) a **G** (great) slouží pro znaménkové proměnné.
Pro negaci **N** (not) a pro rovnost **E** (equal).
Speciální význam mají porovnávání **JZ/JNZ** porovnává zero flag, **JC/JNC** cary flag, **JO/JNO**, **JS/JNS**

**Příklad:**
``` 
CMP eax, ebx	; porovna obsahy promenych
JA label		; if (eax > ebx) { jump label; } 
```
```
CMP edx, [length]
JB label		; if (edx < length) { jump label; }
```

--------------------------
Př 04
------------------------

### Funkce
* Registry EAX, ECX a EDX lze libovolně ve funkci libovolně ukládat
* Registry EBX, ESI a EDI - tyto registry musí být na konci funkce ve stejném stavu jako na začátku funkce, jsou určeny pro vnitřní použití procesoru a musí být na konci funkce ve stejném stavu jako na začátku
* Návratová hodnota funkce se vrací v akumulátoru AL, AH, AX, EAX
* Pro vkládání argumentů jsou použity zásobníkové registry ESP, EBP
* EBP je pozice vršku zásobníku společně s lokálními proměnnými
* ESP je pozice vršku zásobníku pouze s parametry funkce a X atributem
* Před ukončením funkce se musí vrátit pozice EBP zpět na ESP - lokální proměnné jsou zapomenuty

* Každá funkce musí začínat příkazem `ENTER N, M`
	* kde M je úroveň zanoření funkce, pro nás téměř vždy 0
	* N je pro lokální proměnné, většinou může být 0

* Funkce končí příkazem `LEAVE` a následně `RET`

* Parametry funkce se v C předávají zprava do leva, tzn. nejlevější funkce je na spodku zásobníku

1. naplnění zásobníku hodnotami
2. zavolání funkce pomocí `CALL`
3. začátek funkce `ENTER` 
4. vytažení dat ze zásobníku
5. vložení návratové hodnoty do akumulátory
6. úklid zásobníku

Př 05
--------------------------

**LEA reg, [ebp + 8]**
Umožňuje vycucnout argumenty předávané zásobníkem
Umožnuje také jednoduché násobení `LEA EAX, [EAX * 3]` vynásobení čísla konstantou bez nutnosti využívat instrukce imul, kt. potřebuje předpřipravit akumulátor

Přístup k lokální proměnné 
```
LEA EBX, [EBP - 100]
PUSH EBX
CALL STRLEN
```
ve funkci strlen by pak musí být nastavena velikost interní proměnné `ENTER 100, 0`

**TEST**
Provede logický AND, ale neuloží výsledek do registru, nastaví pouze flagy pro porovnávání

SHL/SAL cíl, hodnota
Provádí aritmetický bitový posun, stejný jako běžný bitový posun, ale číslo kt z registru vypadne se nezahodí, ale nastaví se s tím zeroflag.

**SAR**
Aritmetický posun doprava, používá se při posunu záporných čísel. Pokud bychom provedli běžný posun, vlevo by se nám automaticky nasunula 0, což by porušilo zápis záporného čísla a stalo se z něj kladné číslo.
Instrukce SAR nasune doleva stejné číslo jaké předtím bylo na nejlevější pozici.

**Bitová rotace ROL/ROR**
Bity v registru se točí dokola, při rotaci doprava se všechny bity přesunou doprava a ten který úplně napravo vypadne se zařadí zpět doleva

**RCL/RCR**
Bitová rotace s carryflagem, při rotaci doprava spadne pravý bit do carryflagu a do nejlevějšího bitu se nasune hodnota z carryflagu.
používá se pokud chceme rotovat mezi sebou více registrů.

**BT reg, num**
Otestuje bit na pozici num a podle toho zda je 0/1 nastaví carryflag.
```
BT eax, 7	; zkopíruje bit na 7 pozici do carryflagu, kde ho poté můžeme otestovat
```

**SHRD/SHLD**
Nasouvá bity z jednoho registru do druhého registru, ale nemění hodnoty ve zdrojovém registru.

**NEG**
Zneguje registr, obrátí celý znaménkový rozsah, nejen horní bit.

### Řetězcové instrukce

**EDI** - destination registr
**ESI** - source registr
**ES/DS**- segmentové registry, oba jsou 16 bitové

Nelze udělat `MOV ES, DS` nelze mezi nimi přesouvat, musí se přes nějaký jiný registr.
```
MOV AX, DS
MOV ES, AX
; nebo
PUSH DS
POP ES
```

**B/W/D** 

**B** - typ char
**W** - word
**D** - číslo

**DF**

Direction flag, určuje směr, jakým se budeme v řetězci posouvat.

**REP**
Opakovače - obdoba loopu s řetězci, provádí zároveň kontrolu zeroflagu

**MOVSB/W/D**
Přesune bit z jednoho pole do druhého pole

Použití MOVSB/W/D společně s REP:

```
// rep mvsb
while (ecx) { 
	ecx--; 
	*edi = *esi; // kopiruje z místa kde ukazuje esi do kde ukazuje edi
	edi = edi + 1 - 2 * df; // protože df = 1 je decrement; 1 - 2 * 1 = 1-2 = -1
	esi = esi + 1 - 2 * df;
}
```

**LODSB/W/D**
Přesune z pole bit do akumulátoru, kde si ho můžu nějakým způsobem vyhodnotit.

**SCASB/W/D**
Obdoba CMP pro pole. Lze použít pro nalezení rozdílu mezi poli, nebo první výskyt znaku v řetězci.
**!!!** Pozor zdrojová a cílová adresa je u této instrukce opačně

**CMPSB/W/D**
Porovnávání řetězců.

Př 06
-----------------------------

### 64 bitový režim
V 64 bitovém režimu jsou registry rozšířené na 64 bitů, registry se značí RAX, RBX, RCX, ...

Nově přibylo také 8 nových registrů R8 - R15

V 64 bitovém režimu není float point režim, používá se výhradně SSE.

Parametry se předávají v sadě registrů **RDI, RSI, RDX, RCX, R8, R9**, parametry se stále z C vyhodnocují zprava, ale v registrech jsou počítány zleva, tzn. úplně nejlevější parametr funkce bude v registru RDI.

Kompilátor parametry nejdříve projde (<--) a spočítá je a následně jimi naplní registry a zásobník ve směru -->

Floatpoint se předává pomocí SSE v XMM0 až XMM7.

> Př:
> předáváme funkci fce(int, int, float, float)
> hodnoty budou uloženy v RDI, RSI a XMM0 a XMM1
> fce(int, float, int, float) -> RDI, XMM0, RSI, XMM1

Pokud předáváme více než 6 argumentů, 7. bude uložený na prvním místě v zásobníku

**!!!** Pokud víme, že se počet předaných argumentů vleze do 6 vyhrazených registrů, není potřeba na záčátku funkce volat `enter 0,0` a na konci `leave` (nutné použít pouze pokud bereme argumenty se zásobníku).

Čtení 64bitových hodnot se zásobníku:

```
enter 0,0
mov rdx, [ rbp + 16 ]	; rdx = první hodnota na 64bitovém zásobníku (vršeno po 8bytech)
mov rax, [ rbp + 24 ]	; rax = druhá hodnota na 64bitovém zásobníku
leave
ret
```

**!!!** Všechny adresy a pointery jsou 64 bitové, adresy se nepřičítají po 4, ale po 8.

Hodnoty v registrech RBX, R12, R13, R14 a R15 se musí obnovovat, tzn. musí být na konci funkce stejné jako při volání funkce

před dělením provádíme znaménkové rozšíření pomocí **cqo** funkce.



Průchod řetězci v 64 bitovém režimu je indikován pomocí **q**, např **scasq**.

V 64 bitovem režimu nefuguje POPA a PUSHA, musí se každý registr pushnout zlvášť

	
Př 07
-------------------
### Násobení velkých čísel

#### 64bitů
(a * b) = (AH + AL) * (BH + BL)

#### N bitová čísla
každách 1000 dekadických  řádků je cca 10 * 3 binárních

Př 09
---------------------

### Bitový posun
**SHRD/SHLD cíl, zdroj, kolik**
Provede bitový posun mezi 2 registry, ze zdroje se bity vysouvají, do cíle.
Zdroj se posuvem nijak nemění.


Př 10
----------------
### Fixed point

Kvůli neslučitelnosti dekadické a binární soustavy je problém používání floatů, např. dekadické 0.1 = 0.00011 periodických binárních což způsobuje postupem času velké nepřesnosti

Fixed point formát je nedesetiné číslo u kt. jsme si rozdělili na desetinou a nedesetinou část

Obvykle zapisujeme jako 16:4, 32:8, 32:16, ... kde první část je reálná druhá desetiná
většinou se používají binární násobky

Dva fixed poin čísla lze sčítat a odečítat bez dalších úprav

#### Násobení
A * B = A * Z + B * Z = A * B * Z * Z = C * Z

C = (A * B) / Z

Nejdříve musím číslo vynásobit nějakým desetiným posuvem Z.
výsledek násobení pak musím vydělit posuvem Z.

#### Dělení
A / B = C

A / B = (A * Z) / (B * Z)

(A * Z) / B = C

Př 11
---------------

### SSE jednotka
Subsystém procesoru pro práci se speciálními aritmetickými operacemi

V 32 bitovém režibu měla puze 8 registrů a rozlišení float
V 64 režimu má 16 registrů velikosti 128b

#### 64 bitový režim

Do registru lze uložit jeden float, jeden double, 4 floaty, nebo 2 doubly

Zarovnání výsledku - některé instrukce vyžadují aby data v paměti byla zarovnána na adresy kt. jsou násobky 16

Skalární hodnoty - nevyplňují celý 128b registr, pouze jeho část
Paketové hodnoty - vyplňují celých 128b, dva doubly, nebo 4 floaty

**SHUFxx cíl, zdroj, maska**

xx = PS -> kopírování paketu s floaty, [intel doc](http://www.jaist.ac.jp/iscenter-new/mpc/altix/altixdata/opt/intel/vtune/doc/users_guide/mergedProjects/analyzer_ec/mergedProjects/reference_olh/mergedProjects/instructions/instruct32_hh/vc293.htm)
xx = PD -> kopirovani paketu s doubly, [intel doc](http://www.nacad.ufrj.br/online/intel/vtune/users_guide/mergedProjects/analyzer_ec/mergedProjects/reference_olh/mergedProjects/instructions/instruct32_hh/vc292.htm)

**MAXPS cíl, zdroj**
Porovná cil a zdroj a do cíle uloží minimum z obou. [intel doc](http://www.felixcloutier.com/x86/MAXPS.html)


Př 12
-----------------------

### Rozdíly C vs C++

V C lze deklarovat proměnné jenom v těle funkce za závorkou
v C++ lze deklarovat proměnné kdekoliv
Vnitřně je předávání ale stejné, alokuje se na zásobník

V C++ je navíc ještě možné předávat funkce pomocí &parametr, jedná se o převlečená pointer, který se se jenom uživateli usnadňuje práci, protože nemusí provádět dereferenci
V Asm se ale zachází s parametrem stejně jako s pointerem

V C++ je možné přetěžovat funkce
Vzhledem k tomu že nelze přetěžovat v ASM funkce musí C++ překladač přetížené funkce přeložit na jiné jména
Proces přejmenovávání není standardizován závisí na překladači

V C++ je možné mít předinicializované parametry v deklaraci funkce, tyto parametry musí být nasázeny zprava
V ASM to nehraje roli, překlad na korektní funkci zařídí překladač

C++ je rozšířeno o třídy



#### Přetěžování

Existují v podstatě 2 možnosti:

1. nadefinuje v C++ kódu blok s pravidly C
2. upravíme funkce v ASM tak aby se jmenovali stejně jako je překládá C++ překladač, což může být dost nevyspitatelné 
    
Pokud chceme v C++ definovat funkce Cčkovským způsobem musíme je uzavřít do bloku extern "C"

```
#ifdef __cplusplus
extern "C" {
#endef

// tady je blok kodu v C

#ifdef __cplusplus
}
#endef
```

Pokud spustíme v C překladači blok ifdef __cplusplus je ignorován 

#### Třídy

##### Překlad proměnné this
this je vnitřně implementován jako skrytý nultý parametr funkce, takže
a.printí() se přeloží na print(&a);

překladač pochopitelně provede i přejmenování funkce print, tak aby se neduplikovala s nějakými stejně pojmenovanými funkcemi z jiných tříd a interními funkcemi

##### Public / Private

v binární bodobě mezi tím není žádý rozdíl, všechny přístupové práva řeší pouze překladač C++

##### Virtální metody

V konstruktoru nefunguje volání virtuálních metod, protože nemsusí být všechny funkce ještě definovány
Často se to obchází tak že si tvůrci knihoven vytvoří inicializační metodu init

Seznam arametrů se předává jako pole ukazatelů na funkce

### Java