	bits 32

	section .data

	section .text

;;; summ of 32b and 8b num - WORKING
	global sum_32_8
sum_32_8:
	enter 0,0

	mov eax, [ebp + 8]
	mov ecx, 0
	mov cl, byte [ebp + 12]

	add eax, ecx

	leave
	ret

;;; sum of 32b and 16b num - WORKING
	global sum_32_16
sum_32_16:
	enter 0,0

	mov eax, [ebp + 8]
	mov ecx, 0
	mov cx, word [ebp + 12]

	add eax, ecx

	leave
	ret

;;; call sum of ...
	global call_sum_32_16
call_sum_32_16:
	enter 0,0

	mov eax, [ebp + 8]
	mov ecx, 0
	mov cx, word [ebp + 12]

	push cx		; push second param
	push eax	; push first param
	call sum_32_16	; call sum function

	leave
	ret		; return eax value from external function

;;; sum of 64b and 32b num - WORKING
	global sum_64_32
sum_64_32:
	enter 0,0

	mov eax, [ebp + 8]
	mov edx, [ebp + 12]
	mov ecx, [ebp + 16]

	add eax, ecx
	adc edx, 0

	leave
	ret

;;; strlen function - WORKING 
	global strlen_custom
strlen_custom:
	enter 0,0

	push edi
	push esi

	mov edi, [ebp + 8]
	mov esi, edi	; prokopiruje odkaz na retezec do obou registru
	xor eax, eax
	xor edx, edx	; delka = 0

.back:
	lodsb	; nacte jeden byte
	test al, al	; otestuje jestli je \0
	jz .end	;pokud ano - konec
	inc edx ; jinak delka++
	jmp .back ; dalsi pruchod
.end:
	mov eax, edx ; vraci vyslednou delku
	
	pop esi
	pop edi

	leave
	ret

;;; find max in long long array - NOT WORKING
global maximumOfLongArr
maximumOfLongArr:
	enter 0,0
	push edi
	push esi
	push ebx

	mov edi, [ ebp + 8 ] ; pole? mozna voe
	mov ecx, [ ebp + 12 ] ; delka pole
	mov eax, [ edi + ecx * 8 - 8 ];
	mov edx, [ edi + ecx * 8 - 4 ]
	dec ecx
.loop:
	mov ebx, [ edi + ecx * 8 - 8 ];upper part from number
	mov esi, [ edi + ecx * 8 - 4 ]; lower part from number
	
	cmp eax, ebx
	jl .this
	; je .this2
	loop .loop
	jmp .end
	
.this:
	mov eax, ebx
	mov edx, esi
	loop .loop
.this2:
	cmp edx, esi
	jl .this
	loop .loop
.end:

	pop ebx
	pop esi
	pop edi
	leave
	ret

;;; find max in long long array - WORKING
	global max_of_longlong_arr
max_of_longlong_arr:
	enter 0,0
	push edi
	push esi
	push ebx

	mov edi, [ebp + 8]
	mov ecx, [ebp + 12]
	
	; init max value with last item in array
	mov edx, [ebp + ecx * 8 - 4] ; lower part
	mov eax, [ebp + ecx * 8 - 8] ; upper part
	dec ecx

.back:
	mov ebx, [edi + ecx * 8 - 4] ; lower part
	mov esi, [edi + ecx * 8 - 8] ; upper part

	cmp esi, eax
	jle .skipupper
	mov eax, esi
	mov edx, ebx
	jmp .next
.skipupper:
	cmp ebx, eax
	jle .next
	mov eax, esi
	mov edx, ebx
.next:
	loop .back

	pop ebx
	pop esi
	pop edi

	leave
	ret


;;;
	global sumSdelitelem
sumSdelitelem:
	enter 0,0
	push edi
	push esi
	mov ecx, [ ebp + 12 ] ;delka pole
	mov ebx, [ ebp + 8 ]; *array
	mov esi, [ ebp + 16]; delitel
	xor edi, edi

.back:
	mov eax, [ ebx + ecx * 4 - 4 ];nactu cislo do eax
	cdq
	idiv esi
	cmp edx, 0
	jne .next
	add edi, [ ebx + ecx * 4 - 4 ]

.next:
	loop .back

	mov eax, edi
	pop esi
	pop edi
	leave
	ret


;;;
global nasob_64_32
nasob_64_32:
	enter 0,0
	mov eax, [ebp + 12]
	mul dword [ebp + 16]
	mov ecx, eax
	mov eax, [ebp + 8]
	mul dword [ebp + 16]
	add edx, ecx
	leave
	ret


;;;
	global longlong_div2
longlong_div2:
	enter 0,0

	mov eax, [ebp + 8] ; lower part
	mov edx, [ebp + 12] ; upper part

	clc
	rcr edx, 1
	rcr eax, 1

	leave
	ret

;;;
	global satur_plus
satur_plus:
    enter 0,0
 
    mov eax, [ebp+8] 	; a
    mov edx, [ebp+12] 	; b
    mov ebx, eax 		; tmp_a = a
    mov ecx, 0
 
    add ebx, edx 		; tmp_a += b
 
    cmp eax, ecx		;
    jge .pos			; if (a >= b) goto .pos;
    cmp edx, ecx		; 
    jge .posboth		; if (b >= 0) goto .posboth;
    cmp ebx, ecx		; if ( a < a+b) goto end;
    jl .end
    mov ebx, 0x80000000
    jmp .end
.pos:
    cmp edx, ecx
    jl .end
.posboth:
    cmp ebx, ecx
    jge .end
    mov ebx, 0x7FFFFFFF
.end:
    mov eax, ebx
    leave
    ret