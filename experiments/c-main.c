
#include <stdio.h>

int sum_32_8(int i, char c);
int sum_32_16(int i, short s);
int call_sum_32_16(int i, short s);
int strlen_custom(char *str);
long long sum_64_32(long long l, int i);
long long max_of_longlong_arr(long long *arr, int len);
long long maximumOfLongArr(long long *arr, int len);
int sumSdelitelem(int *arr, int len, int delitel);
long long nasob_64_32(long long l, int i);
int longlong_div2(long long i);

int main()
{
	printf("%d\n", sum_32_8(1000,64));
	printf("%d\n", sum_32_16(1000,1));
	printf("Call sum: %d\n", call_sum_32_16(1000,64));
	printf("%Ld\n", sum_64_32(2147483647, 64));
	printf("%d\n", strlen_custom("ahoj jak se mas"));

	long long arr[4] = {17,-20,-30,14};
	long long array[2] = {15641651,15968};
	printf("%Ld\n", max_of_longlong_arr(arr, 4));
	// printf("%Ld\n", maximumOfLongArr(array, 2));	

	printf("Nasob 64: %Ld\n", nasob_64_32(5000000000, 2));
	printf("Div longlong 2 %d\n", longlong_div2(7));
	
	return 0;
}
