#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define SIZE 8


int shl_intN( int *iN, int N );
int shr_intN( int *iN, int N );
void shrd_intN( int *iN, int okolik, int N );
void shld_intN( int *iN, int okolik, int N );
void add_intN_intN( int *aN, int *bN, int N );
void sub_intN_intN( int *aN, int *bN, int N );

long long mul_i64_i32( long long a, int b );
long long div_i64_i32( long long a, int b, int *rem );
long long shrd_i64( long long a, int okolik );
long long shld_i64( long long a, int okolik );
// intN = intN / int32
int div_intN_int32( int *intN, int int32, int N );
int add_intN_int32( int *intN, int int32, int N );
int mul_intN_int32( int *intN, int int32, int N );

void str_mirror( char *str )
{
	int l = strlen( str );
	char tmp[ l + 1 ];
	for ( int i = 0; i < l; i++ )
		tmp[ l - i - 1 ] = str[ i ];
	tmp[ l ] = 0;
	strcpy( str, tmp );
}

int is_zero( int *intN, int N )
{
	for ( int i = 0; i < N; i++ )
		if ( intN[ i ] ) return 0;

	return 1;
}

// compare of two intN numbers
// a >  b return  1
// a == b return  0
// a <  b return -1
int cmp_intNintN( int *a, int *b, int N )
{
  int i;
  for ( i = N - 1; i >= 0; i-- )
  {
	  if ( a[ i ] == b[ i ] ) continue;
	  if ( ( unsigned int ) a[ i ] > ( unsigned int ) b[ i ] ) return 1;
	  return -1;
  }
  return 0;
}

void mul_intN_intN( int *aN, int *bN, int N )
{
	int res[ N ];
	bzero( res, sizeof( res ) );

	for ( int i = 0; i < N * 32; i++ )
	{
		if ( bN[ 0 ] & 1 )
		{
			add_intN_intN( res, aN, N );
		}
		shl_intN( aN, N );
		shr_intN( bN, N );
	}

	memcpy( aN, res, sizeof( res ) );
}

// div N size numbers
void div_intN_intN( int *aN, int *bN, int N )
{
	int delenec[ 2 * N ];
	int res[ N ];
	bzero( res, sizeof( res ) );
	bzero( delenec, sizeof( delenec ) );
	memcpy( delenec, aN, N * sizeof( int ) ); // delenec = aN
	int *scena = delenec + N;

	for ( int i = 0; i < N * 32; i++ )
	{
		shl_intN( delenec, 2 * N );
		shl_intN( res, N ); 
		if ( cmp_intNintN( scena, bN, N ) >= 0 )
		{
			res[ 0 ] |= 1;
			sub_intN_intN( scena, bN, N );
		}
	}
	memcpy( aN, res, N * sizeof( int ) );
	memcpy( bN, scena, N * sizeof( int ) ); // zbytek
}

// div 32 bit numbers
int div_int32_int32( int a, int b )
{
	long long delenec = a;  // delenec int[ 2 ]
	int *scena = ( int * ) &delenec;
	scena++;                // &int[ 1 ]
	int res = 0;

	for ( int i = 0; i < 32; i++ )
	{
		res <<= 1;
		delenec <<= 1;
		if ( *scena >= b )
		{
			res |= 1;
			*scena -= b;
		}

	}
	return res;
}

// TASK 1
// N size sqrt
void sqrt_intN(int *array, int N)
{
	int delenec[N * 2];
	int res[N];
	bzero(res, sizeof(res));
	bzero(delenec, sizeof(delenec));

	memcpy(delenec, array, N * sizeof(int));
	int *scena = delenec + N;

	int B[N];
	for (int i = 0; i < N * 16; i++)
	{
		shld_intN(delenec, 2 , N*2);
		shld_intN(res, 1, N);

		memcpy(B, res, sizeof(int) * N);

		mul_intN_int32(B, 2, N);
		add_intN_int32(B, 1, N);
		if (cmp_intNintN(scena, B , N) >= 0)
		{
			res[0] |= 1;
			sub_intN_intN(scena, B, N);
		}
	}
	memcpy(array, res, sizeof(int) * N);
}

// simple 32bit sqrt
int sqrt_int32( int a )
{
	long long delenec = a;
	int *scena = ( int * ) &delenec;
	scena++;
	int res = 0;

	for ( int i = 0; i < 16; i++ )
	{
		res <<= 1;
		delenec <<= 2;
		int B = res * 2 * 1 + 1 * 1;
		if ( *scena >= B )
		{
			res |= 1;
			*scena -= B;
		}
	}
	return res;
}

char *intN_to_str( int *intN, char *str, int N )
{
	char *tmp = str;
	while ( !is_zero( intN, N ) )
	{
		int zbytek = div_intN_int32( intN, 10, N );
		*str = zbytek + '0';
		str++;
	}
	*str = 0;

	str_mirror( tmp );
}

char *str_to_intN( char *str, int *intN, int N )
{
	for ( int i = 0; i < N; i++ )
		intN[ i ] = 0;
	while ( *str )
	{
		mul_intN_int32( intN, 10, N );
		add_intN_int32( intN, *str - '0', N );
		str++;
	}
}

void euler_num()
{
	int N = 1040;
	int Ns = 10010;
	int Z[ N ], Zc[ N ], fakt[ N ], faktc[ N ];
	char Zs[ Ns ];
	char vysls[ Ns ];
	int vysl[ N ];

	bzero( vysl, sizeof( vysl ) );
	memset( Zs, '0', sizeof( Zs ) ); // "0000.."
	Zs[ 10001 ] = 0;
	Zs[ 0 ] = '1';                   // "10000...\0"
	str_to_intN( Zs,  Z, N );
	memcpy( vysl, Z, sizeof( Z ) ); // vysl = 1
	mul_intN_int32( vysl, 2, N );   // vysl = 2
	bzero( faktc, sizeof( faktc ) );
	faktc[ 0 ] = 1;                 // fakt=1
	
	for ( int i = 2; i < 1000; i++ )
	{
		printf( "i=%d\n", i );
		mul_intN_int32( faktc, i, N );
		memcpy( Zc, Z, sizeof( Z ) );
		memcpy( fakt, faktc, sizeof( fakt ) );
		div_intN_intN( Zc, fakt, N );
		add_intN_intN( vysl, Zc, N );
	}

	intN_to_str( vysl, vysls, N );
	printf( "e=%s\n", vysls );

}

// TASK 2 vypocet PI
// Pi = Suma<0-iterations>((1/16^k) * ((4 / 8*k + 1) - (2 / 8*k + 4) - (1 / 8*k + 5) - (1 / 8*k + 6)))
void pi(int iterations)
{
	int N = 1040;
	int N_str = 10010;

	int pi[N], up[N], p0[N], tmp_p0_down[N], p0_down[N], p1[N], p2[N], p3[N], p4[N];
	char up_str[N_str], pi_str[N_str];

	bzero(pi, sizeof(pi)); // vynuluje pi

	memset(up_str, '0', sizeof(up_str)); // naplni up_str string nulami
	up_str[5001] = 0; // na 10001. pozici vlozi nulu kt. indikuje konec retezce
	up_str[0] = '1'; // na prvni pozici vlozi char 1, tzn. v retezci bude ulozeno cislo zacinajici 1 nasledovano 1000 nulami

	str_to_intN(up_str, up, N); // prevede zaklad horni casti polynomu z retezce na pole cisel

	bzero(p0_down, sizeof(p0_down)); // vynuluje dolni cast polynomu p0
	add_intN_int32(p0_down, 1, N); // nastaveni delitele polynomu p0 na hodnotu 1 - kazdem kroku cyku bude posunut o 4 bitove pozice << (na konci 1. kroku vznikne 16)

	for (int k = 0; k < iterations; k++)
	{
		memcpy(p0, up, sizeof(up)); // kopirovani horni casti polynomu do p0
		memcpy(tmp_p0_down, p0_down, sizeof(p0_down)); // prekopirovani dolni casti polynomu p0 za ucelem deleni
		div_intN_intN(p0, tmp_p0_down, N); // (100000....0) / 16^k 

		memcpy(p1, up, sizeof(up));
		mul_intN_int32(p1, 4, N);
		div_intN_int32(p1, ((8 * k) + 1), N); // p1 = (40000...0) / 8*k + 1

		memcpy(p2, up, sizeof(up));
		mul_intN_int32(p2, 2, N);
		div_intN_int32(p2, ((8 * k) + 4), N); // p2 = (200000...0) / 8*k + 4

		memcpy(p3, up, sizeof(up));
		div_intN_int32(p3, ((8 * k) + 5), N); // p3 = (100000...0) / 8*k + 5

		memcpy(p4, up, sizeof(up));
		div_intN_int32(p4, ((8 * k) + 6), N); // p4 = (1000000...0) / 8*k + 6

		sub_intN_intN(p1, p2, N); // p1 = p1 - p2
		sub_intN_intN(p1, p3, N); // p1 = p1 - p3
		sub_intN_intN(p1, p4, N); // p1 = p1 - p4

		mul_intN_intN(p0, p1, N); // p0 = p0 * (p1 - p2 - p3 - p4)
		add_intN_intN(pi, p0, N); // pi += p0

		// test print
		// char tmp[N_str];
		// int tmp_p[N];
		// memcpy(tmp_p, pi, sizeof(pi));
		// intN_to_str(tmp_p, tmp, N);
		printf("iteration: %d\n", k + 1);

		shld_intN(p0_down, 4, N); // bitovy posun doleva zajisti pronasobeni dolni casti polynomu p0 - ekvivalent p0_down * p0_down
	}

	intN_to_str(pi, pi_str, N);
	printf("Pi: %s\n", pi_str);
}

/*
*	TASKS:
*	1) upravit odmocnovani pro cisla delky N
*	2) predelat vypocet eulerova cisla na vypocet pi pomoci metody Spigot
*/
int main()
{
	// test obycejneho int32 deleni a na
	printf("deleni int32/int32: %d\n", div_int32_int32(600, 3));
	printf("odmocnina int32: %d\n", sqrt_int32(90000));

	// N rozmerne deleni
	int num1[4] = {0, 0, 0, 600};
	int num2[4] = {0, 0, 0, 3};
	div_intN_intN(num1, num2, 4);
	printf("deleni intN/intN: ");
	for (int i = 0; i < 4; i++)
		printf("%d ", num1[3 - i]);
	printf("\n");

	// TASK 1
	// N rozmerne odmocneni
	int num3[4] = {90000, 0, 0, 0};
	sqrt_intN(num3, 4);
	printf("odmocnina intN: ");
	for (int i = 0; i < 4; i++)
		printf("%d ", num3[3 - i]);
	printf("\n");

	pi(500);
}

int main_fakt()
{
	int cislo[ 270 ];
	char vysl[ 8600 ];

	bzero( cislo, sizeof( cislo ) );
	cislo[ 0 ] = 1;
	for ( int i = 2; i <= 1000; i++ )
		mul_intN_int32( cislo, i, 270 );

	intN_to_str( cislo, vysl, 270 );
	printf( "1000!=%s\n", vysl );
}

int main1()
{
	int cislo[ 4 ] = { 0xcafecafe, 0xbeefbeef, 0xcafecafe, 0xbeefbeef };
	printf( "odmocnina %d\n", sqrt_int32( 100000000 ) );
	printf( "shrd %Ld\n", shrd_i64( 320000000000, 4 ) );
	printf( "shld %Ld\n", shld_i64( 100000000000, 6 ) );

	shrd_intN( cislo, 16, 4 );
	for ( int i = 0; i < 4; i++ )
		printf( "%08X ", cislo[ 3 - i ] );
	printf( "\n" );
}


int main0()
{
	char sn1[ 100 ] = "123456789123456789123456789";
	char sn2[ 100 ] = "10000000000";
	char sres[ 100 ];

	int n1[ 8 ], n2[ 8 ];
	str_to_intN( sn1, n1, 8 );
	str_to_intN( sn2, n2, 8 );

	mul_intN_intN( n1, n2, 8 );
	intN_to_str( n1, sres, 8 );
	printf( "nasobeni %s\n", sres );

	str_to_intN( sn1, n1, 8 );
	str_to_intN( sn2, n2, 8 );
	div_intN_intN( n1, n2, 8 );
	intN_to_str( n1, sres, 8 );
	printf( "deleni %s\n", sres );
	intN_to_str( n2, sres, 8 );
	printf( "deleni %s\n", sres );

}

int mmain()
{
	int n1[ 8 ] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
	int n2[ 8 ] = { 0x10000, 0, 0, 0, 0, 0, 0, 0 };
	//add_intN_intN( n1, n2, 8 );

	printf( "deleni %d\n", div_int32_int32( 10, 3 ) );

	div_intN_intN( n1, n2, 8 );

	for ( int i = 0; i < 8; i++ )
		printf( "%08X ", n1[ 7 - i ] );
	printf( "\n" );
	for ( int i = 0; i < 8; i++ )
		printf( "%08X ", n2[ 7 - i ] );
	printf( "\n" );
}
