	bits 32

	section .data

	nega	dd	0
	posi	dd	0
	tmp		dd 0

	section .text

	global count_positive_negative
	global add64
	global toLow
	global strlength
	global strcompare
	global asm_memmove
	global reverse_copy_test
	global sum_to_longlong
	extern debug


;;;
;;; TASKS
;;;

;;; Task 1.1 from CV06
	global infinit_sum32
infinit_sum32:
	enter 0,0
	xor eax, eax
	xor ecx, ecx
.while:
	mov edx, [ ebp + 8 + ecx]
	cmp edx, 0
	je .done
	add eax, edx

	add ecx, 4
	jmp .while

.done:
	leave
	ret


;;; Task 1
;;; sum of 32 bits extend to 64 bit num
sum_to_longlong:
	enter 0,0
	push ebx

	mov ebx, [ebp + 8]		; load array
	mov ecx, [ebp + 12]		; load array size

	xor eax, eax
	xor edx, edx
.while:
	add eax, [ebx + ecx * 4 - 4]
	; cdq

; 	cmp eax, 0
; 	jge .positive
; 	mov [tmp], edx
; 	cdq
; 	adc edx, [tmp]
; 	jmp .done
; .positive:

	adc edx, dword 0		; edx = 0 + carry_flag

.done:
	loop .while

	pop ebx
	leave
	ret

;;; task 2
;;; memove
asm_memmove:
	enter 0,0
	push edi
	push esi


	mov edi, [ebp + 8]		; destination string
	mov esi, [ebp + 12]		; source string
	mov ecx, [ebp + 16]		; copy length

	mov ax, ds				; some register magic ???
	mov es, ax				; 

	cmp esi, edi			
	jl .reverse				; if src_start_position < dest_start_position then jump .reverse

	cld						; set direction flag to 0 direction ->
	rep movsb				; copy from left to right
	jmp .done

.reverse:
	std						; set direction flag to 1 direction <-

	add edi, ecx
	dec edi					; source_start_index = source_start_index + copy_length - 1
	add esi, ecx
	dec esi					; dest_start_index = dest_start_index + copy_length - 1

	rep movsb

.done:
	mov eax, dword 0
	pop esi
	pop edi
	leave
	ret

;;; task 3
;;; pocet kladnych a zapornych cisel
count_positive_negative:
	enter 0,0
	push ebx
	push edi

	mov ebx, 0	; positive
	mov edx, 0	; negative

	mov ecx, [ebp + 16]		; array size
	mov edi, [ebp + 20]		; array 
	
	mov eax, 0
.repeat:
	scasd				; cmp eax - EDI; EDI++
						; odecte hodnotu v retezci od akumulatoru a porovna ji ze stavem akumulatoru
						; zaroven posunu aktualni hodnotu retezce od 1 doprava
	setl dl				; if ((eax - EDI[i]) < 0) then edx = 1 else edx = 0; musi se nastavovat 8bitova cast edx registru
	setg bl				; if ((eax - EDI[i]) > 0) then ebx = 1 else ebx = 0; 
	add [nega], ebx		; negative += 1
	add [posi], edx		; positive += 1
	

	loop .repeat

	mov edx, [ebp + 8]	; edx = *negative
	mov eax, [nega]
	mov [edx], eax		; negative = neg_sum; !!! [edx] <=> value; edx <=> pointer address
	mov edx, [ebp + 12]	; edx = *positive
	mov eax, [posi]		
	mov [edx], eax		; positive = pos_sum

	pop edi
	pop ebx
	leave
	ret

;;;;;;;;;;;; add64
add64:
	enter 0,0
	;
	mov eax, [ebp + 8]	; a_low
	add eax, [ebp + 16]	; a_low += b_low
	mov edx, [ebp + 12]	; a_high
	add edx, [ebp + 20]	; a_high += b_high
	;
	leave
	ret

;;;;;;;;; toLow
toLow:
	enter 0,0
	pop edi		; uschova registru kt. se nesmi upravovat
	pop esi

	mov ax, ds
	mov es, ax
	; cld ; nastaveni direction flagu 
	mov edi, [ebp + 8]	; str
	mov esi, edi
.back:
	lodsb				; al = *esi ; vezme jeden znak z esi a automaticky se inkrementuje
	cmp al, 0		; hleda konec retezce
	je .done

	cmp al, 'A'
	jb .nic
	cmp al, 'Z'
	ja .nic
	add al, 'a' - 'A'			; provede minifikaci pismen posunem v ASCII

.nic:
	stosb				; ulozeni upravene hodnoty do puvodniho retezce
	jmp .back

.done:
	mov eax, [ebp + 8]	; nastavi navratovou adresu
	pop esi				; obnova registru kt. se nesmi upravovat
	pop edi				; musi se obnovit v opacneme poradi nez jsme je ulozili
	;
	leave
	ret

;;;;;;;;;;; strlength
strlength:
	enter 0,0
	push edi

	mov edi, [ebp + 8]
	mov al, 0

	mov ecx, 0
	dec ecx		; set ecx to 0xffffffff

repne scasb

	sub edi, [ebp + 8]	; vypocteme delku retezce a odecteme pocatecky adresy
	dec edi				; odecteme jednu pozici kt. vlozil scasb navic na konci retezce

	mov eax, edi

	pop edi

	leave
	ret

	leave
	ret

;;;;;;;;;;;; strcompare
strcompare:
	 enter 0,0
        push edi
        push esi

        push dword [ ebp + 8 ]      ; s1
        call strlength
        add esp, 4
        push eax

        push dword [ ebp + 12 ]     ; s2
        call strlength
        add esp, 4
        pop ecx
        cmp ecx, ebx
        cmovl ecx, eax              ; min( eax, ecx )

        mov ax, ds
        mov es, ax

        mov edi, [ ebp + 8 ]        ; s1
        mov esi, [ ebp + 12 ]       ; s2
repe    cmpsb
        mov eax, 0
        mov esi, -1
        mov edi, 1
        cmovb eax, edi
        cmova eax, esi, 

        pop esi
        pop edi
        leave
        ret


;;;;;;;; Reverse copy test
reverse_copy_test:
	enter 0,0
	push ebx
	push edi
	push esi

	mov ax, ds
	mov es, ax

	mov edi, [ebp + 8]		; destination string
	mov esi, [ebp + 12]		; source string
	mov ecx, [ebp + 16]		; copy length

	; inc ecx
	add edi, ecx
	dec edi
	add esi, ecx
	dec esi
	; cld					; clear direction flag to zero
	std

	rep movsb

	pop esi
	pop edi
	pop ebx
	leave
	ret
