#include <stdio.h>
#include <string.h>

long long add64(long long a, long long b);
char *toLow(char *str);
int strlength(char *str);
int strcompare(char *str1, char *str2);
int count_positive_negative(int *positive, int *negative, int size, int* arr);
void asm_memmove(char *dest, char *source, int size);
void reverse_copy_test(char *dest, char *source, int size);
long long array_sum_64bit(int *arr, int length);
long long sum_to_longlong(int *arr, int length);

int infinit_sum32(int, ...);

int debug;

int main()
{
	// lecture tests
	/*
	printf("add64 %Lx \n", add64(0xFFFFFFFFF, 1));

	char str[] = {"MaLa VElkA PiSMENa 123654789"};
	toLow(str);
	printf("to low %s \n", str);

	printf("strlen c: %d, asm: %d \n", strlen(str), strlength(str));

	printf("strcmp %d\n", strcompare("ahoj", "ahoj5"));
	*/


	// task 1
	int arr1[5]={4000000000, 4000000000, 1000000000, 2000000000, 10};	
	printf("sum: %lld\n", sum_to_longlong(arr1, 5));


	// Task 2
	
	// none overlapping {[dest] [src]}
	// ****src********
	// dst************
	char str1[] = "123456789abcdef";
	reverse_copy_test(str1+6, str1, 2);
	printf("Asm memmove: %s \n", str1);
	
	char str2[] = "123456789abcdef";
	// printf("C memmove: %s, Asm memmove: %s \n", str1, str2);
	memmove(str1, str1+4, 3);
	asm_memmove(str2, str2+4, 3);
	printf("C memmove: %s, Asm memmove: %s \n", str1, str2);

	// none overlapping {[src] [dest]}
	// src************
	// ******dst******
	char str3[] = "123456789abcdef";
	char str4[] = "123456789abcdef";
	memmove(str3+6, str3, 3);
	asm_memmove(str4+6, str4, 3);
	printf("C memmove: %s, Asm memmove: %s \n", str1, str2);

	// overlapping
	// **src**********
	// dst************
	memmove(str1, str1+2, 3);
	asm_memmove(str2, str2+2, 3);
	printf("C memmove: %s, Asm memmove: %s \n", str1, str2);

	// overlapping
	// src**********
	// **dst************
	memmove(str1+2, str1, 3);
	asm_memmove(str2+2, str2, 3);
	printf("C memmove: %s, Asm memmove: %s \n", str1, str2);


	// Task 3
	int positive, negative;
	int arr[5] = {-5, -5, 9, 5, 8};
	count_positive_negative(&positive, &negative, 5, arr);
	printf("Task3 positive: %d, negative %d\n", positive, negative);
	

	// Task 1.1 cv 06
	printf("infinit_sum64 %d \n", infinit_sum32(1,1,1,1,1,1,1,1,1,1,10,0));

}
