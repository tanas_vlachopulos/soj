	bits 32

	section .data

	section .text

;;;
;;; Examples
;;;

;;; multiple big numbers
	global mul_big
mul_big:
	enter 0,0
	push edi

	mov eax, [ebp + 8]	; lower part of long long a
	mul dword [ebp + 16] ; a_l *= big

	mov ecx, eax
	mov edi, edx

	mov eax, [ebp + 12]		; hight part of long long big
	mul dword [ebp + 16]	; a_h *= big

	add edi, eax	; secte mezivysledek z prvniho nasobeni
	mov edx, edi	; prepare return value
	mov eax, ecx	; prepare reteurn value

	pop edi
	leave
	ret

;;; div big numbers
	global div_big
div_big:
	enter 0,0
	push edi

	mov edx, 0		; prepare edx reg
	mov eax, [ebp + 12]		;eax = a_h
	div dword [ebp + 16]	; a_h /= b

	mov ecx , eax			; save mezivysledek

	mov eax, [ebp + 8]		; eax = a_l
	div dword [ebp + 16]	; a_l /= b

	; return zbytek
	mov edi, [ebp + 20]
	mov [edi], edx			; zbytek = edx

	mov edx, ecx			; prepare return value

	pop edi
	leave
	ret

;;; 
	global div_divN_int32
div_divN_int32:
	enter 0,0
	push ebp
	push edi
	
	mov ecx, [ebp + 16]		; ecx = size
	mov ebx, [ebp + 12]		; ebp = int32 ; cislo kt. delime
	mov edi, [ebp + 8]		; edi = intN ; delene pole
	mov edx, 0

.back:
	mov eax, [edi + ecx * 4 - 4]
	div ebx
	mov [edi + ecx * 4 - 4], eax
	loop .back

	mov eax, edx

	pop edi
	pop ebp
	leave
	ret

;;;
	global add_intN_int32
add_intN_int32:
	enter 0,0
	push edi
	push ebx

	mov ecx, [ebp + 16]		; ecx = size
	mov edi, [ebp + 8]		; edi = intN
	mov ebx, 1				; ebp = inx
	mov eax, [ebp + 12]

	cdq
	mov eax, [edi]			; eax = intN[0]
	add eax, dword [ebp + 12] 
	mov [edi], eax

	dec ecx

.back:
	jnc .done			; ukonci vypocty pokud uz neni zadny prenos do vyssiho radu, v tu chvili uz neni co pocitat
	adc [edi + ebx * 4], edx
	inc ebx
	loop .back

.done:
	pop ebx
	pop edi
	leave
	ret

;;;
	global mul_intN_int32
mul_intN_int32:
	enter 0,0
	push ebx
	push edi
	push esi

	mov ecx, [ebp + 16]		; ecx = size
	mov edi, [ebp + 8]		; intN

	mov esi, 0
	mov ebx, 0
.back:
	mov eax, [ebp + ebx * 4]	; N element of array
	mul dword [ebp + 12]		; int[ecx] *= int32
	add eax, esi				; prenos do vysiho radu
	adc edx, 0
	mov esi, edx

	mov [edi + ebx * 4], eax

	inc ebx
	loop .back

	pop esi
	pop edi
	pop ebx
	leave
	ret