//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>

// Variables

// public global variable
int main_x_pub;

// local variable
static int main_x_loc;

// external variable
extern int tool_x_pub;

// Functions

// public function
int main_fun_pub()
{
  main_x_pub = -3;
  main_x_loc = -2;
  tool_x_pub = -1;
  //tool_x_loc = 0; // impossible
}

// local function
static int main_fun_loc()
{
  main_x_pub = 1;
  main_x_loc = 2;
  tool_x_pub = 3;
  //tool_x_loc = 0; // impossible
}

// external function
extern int tool_fun_pub();
extern int tool_fun_loc();


// print function
int print()
{
  printf( "main_x_pub=%d main_x_loc=%d tool_x_pub=%d\n", main_x_pub, main_x_loc, tool_x_pub );
}

int main()
{
  main_fun_loc();
  print();
  main_fun_pub();
  print();
  tool_fun_pub();
  print();
  //tool_fun_loc(); // impossible
  //print();
}
