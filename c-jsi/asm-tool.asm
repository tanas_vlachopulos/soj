;***************************************************************************
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 32

	section .data

; external variable
extern  main_x_pub
extern  main_x_loc

; public variable
global  tool_x_pub

; local variables
tool_x_pub      dd      0
tool_x_loc      dd      0

        section .text

; public label (function)
global  tool_fun_pub

tool_fun_pub:
	enter 0,0

        mov     [ main_x_pub ], dword 40
        ;mov     [ main_x_loc ], dword 30     ; impossible
        mov     [ tool_x_pub ], dword 20
        mov     [ tool_x_loc ], dword 10

        leave
        ret

tool_fun_loc:
	enter 0,0

        mov     [ main_x_pub ], dword 100
        ;mov     [ main_x_loc ], dword 200     ; impossible
        mov     [ tool_x_pub ], dword 300
        mov     [ tool_x_loc ], dword 400

        leave
        ret
