	bits 64

	section .data

	section .text

;;; TASK 01
;;;
	global sum_array_advance
sum_array_advance:
	xorps xmm0, xmm0
	movsx rcx, esi
	shr rcx, 2		; N /= 4

.back:
	addps xmm0, [rdi]
	add rdi, 16
	loop .back

	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000001
	addss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000010
	addss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000011
	addss xmm0, xmm1

	ret

;;; TASK 02
;;; Array min
	global arr_min
arr_min:
	xorps xmm0, xmm0
	movsx rcx, esi
	shr rcx, 2

	movaps xmm0, [rdi]

.back:
	minps xmm0, [rdi]
	add rdi, 16
	loop .back

	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000001
	minss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000010
	minss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000011
	minss xmm0, xmm1

	ret

;;; TASK 02
;;; Array max
	global arr_max
arr_max:
	movsx rcx, esi
	shr rcx, 2

	movaps xmm0, [rdi]

.back:
	maxps xmm0, [rdi]
	add rdi, 16
	loop .back

	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000001	; some shuf magic
	maxss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000010
	maxss xmm0, xmm1
	movaps xmm1, xmm0
	shufps xmm1, xmm1, 0b000000000011
	maxss xmm0, xmm1

	ret

;;; TASK 03
;;; Quadratic equation
	global quadratic_equation
quadratic_equation:
	movss xmm3, xmm1		; D = b
	mulss xmm3, xmm3		; D = b^2
	movss xmm4, xmm0		; tmp = a
	mulss xmm4, xmm2		; tmp = a*c
	addss xmm4, xmm4		; tmp = 2*a*c
	addss xmm4, xmm4		;tmp = 4*a*c
	subss xmm3, xmm4		; D = b^2 - 4*a*c

	sqrtss xmm3, xmm3		; sqrt(D) 

	mov eax, -1
	cvtsi2ss xmm4, eax		; convert eax to xmm4
	mulss xmm1, xmm4		; b = -b

	addss xmm0, xmm0		; a = 2a

	movss xmm5, xmm1
	addss xmm5, xmm3		; -b + sqrt(D)
	divss xmm5, xmm0		; / 2a
	movss [rsi], xmm5		; x2 = (-b + sqrt(D)) / 2a

	subss xmm1, xmm3		; -b - sqrt(D)
	divss xmm1, xmm0		; / 2a
	movss [rdi], xmm1		; x1 = (-b - sqrt(D)) / 2a
	

	movss xmm0, xmm1
	ret

;;; TASK 4
;;; a^x = expf(x*logf(a))
	global asm_exp
	extern expf
	extern logf
asm_exp:
	enter 16, 0					; pripravi si lokalni zasobnik na 16 B

	movapd [rbp - 16], xmm1		; ulozi promennou xmm1 do lokalniho zasobniku, aby ji nemohlo prepsat volani funkce log
	call logf					; volani funkce log s jednim parametrem xmm0, vysledek se opet vrati do xmm0
	movapd xmm1, [rbp - 16]		; obnoveni registru xmm1 ze zasobniku

	mulss xmm0, xmm1			; x*logf(a)
	call expf					; expf(x*logf(a))

	leave
	ret			; return xmm0 = expf(x*logf(a))

;;; TASK 4 - long double version
;;; a^x = expl(x*logl(a))
;;; dont work return inf
	global asm_expl
	extern expl
	extern logl
asm_expl:
	enter 16, 0					; pripravi si lokalni zasobnik na 16 B

	movapd [rbp - 16], xmm1		; ulozi promennou xmm1 do lokalniho zasobniku, aby ji nemohlo prepsat volani funkce log
	call logl					
	movapd xmm1, [rbp - 16]

	mulpd xmm0, xmm1
	call expl

	leave
	ret

;;;
	global sum_of_negative
sum_of_negative:
	mov eax, 0
	cvtsi2ss xmm0, eax

	movsx rcx, esi
.back:
	comiss xmm0, [rdi + rcx * 4 - 4]
	cmc
	adc eax, 0		;
	loop .back

	ret

	global sum_array
sum_array:
	enter 16,0
	xorps xmm0, xmm0		; sum = 0
	movsx rcx, esi			; rcx = size
	shr rcx, 2				; N /= 4
.back:
	addps xmm0, [rdi]
	add rdi, 16				; posun v poli
	loop .back

	movaps [rbp - 16], xmm0
	addss xmm0, [rbp - 12]
	addss xmm0, [rbp - 8]
	addss xmm0, [rbp - 4]
	
	leave
	ret

	global sum
sum:
	addss xmm0, xmm1
	ret

	global retdouble
retdouble:
	ret

	global retfloat
retfloat:
	ret

	global retdouble2
retdouble2:
	cvtss2sd xmm0, xmm0		; konverze floatu na double
	ret

	global retfloat2
retfloat2:
	cvtsd2ss xmm0, xmm0		; konverze double na float
	ret 

	global int2float
int2float:
	cvtsi2ss xmm0, edi		; konverze int na float
	ret