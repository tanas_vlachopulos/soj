// Lab 11
#include <stdio.h>
#include <math.h>

#define size 4

double retdouble(double x);
float retfloat(float x);
double retdouble2(float x);
float retfloat2(double x);
float int2float(int x);
float sum(float a, float b);

float sum_array(float *arr, int N);
int sum_of_negative(float *arr, int N);

// tasks
float sum_array_advance(float *arr, int N);
float arr_min(float *arr, int N);
float arr_max(float *arr, int N);
float quadratic_equation(float a, float b, float c, float *x1, float *x2);
float asm_exp(float a, float x);
long double asm_expl(long double a, long double x);

long double c_exp(float a, float x)
{
	return expl(x*logl(a));
}

int main()
{
	// printf("double %f\n", retdouble(1.25 / 2));
	// printf("float %f\n", retfloat(3.1415));

	// printf("double2 %f\n", retdouble2(3.13));
	// printf("float2 %f\n", retfloat2(3.1415));

	// printf("int to float %f\n", int2float(5));
	
	// printf("sum %f\n", sum(25.2, 25.3));

	float arr[8] = {1, 1, 2, 2, 1.5, 1.5, 1.2, 0.2};
	printf("array sum %f\n", sum_array(arr, 8));
	printf("negative %d\n", sum_of_negative(arr, 8));

	// Task 1
	printf("advance sum: %f\n\n", sum_array_advance(arr, 8));
	
	// Task 2
	printf("array min: %f\n", arr_min(arr, 8));
	printf("array max: %f\n\n", arr_max(arr, 8));

	// Task 3
	float x1, x2;
	quadratic_equation(1, 3, -4, &x1, &x2);
	printf("x1 = %f, x2 = %f\n", x1, x2);
	quadratic_equation(1, 1, -1, &x1, &x2);
	printf("x1 = %f, x2 = %f\n", x1, x2);
	quadratic_equation(1, 56, 8, &x1, &x2);
	printf("x1 = %f, x2 = %f\n\n", x1, x2);

	// Task 4
	// printf("exp: %Lf \n", c_exp(13,8));
	printf("13^5 in C:   %f \n", expf(5*logf(13)));
	printf("13^5 in Asm: %f \n\n", asm_exp(13, 5));

	printf("long double 13^5 in C:   %Lf \n", expl(5*logl(13)));
	printf("long double 13^5 in Asm: %Lf \n", asm_expl(13, 5));

}
