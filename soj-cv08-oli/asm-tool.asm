;***************************************************************************
;
; Program for education in subject "Assembly Languages"
; petr.olivka@vsb.cz, department of computer science, VSB-TUO
;
; Example of combining module in C with module in Assembly
;
;***************************************************************************
	bits 32

	section .data

        section .text

        global shr_intN
shr_intN:
        enter 0,0
        
        mov ecx, [ ebp + 12 ];      ; N
        mov edx, [ ebp + 8 ]        ; iN
        clc
        ;bt [ edx ], 0              ; mozna uprava
                                    ; na rotaci

.back:
        rcr dword [ edx + ecx * 4 - 4 ], 1
        loop .back

        mov eax, 0
        ;adc eax, 0
        setc al
        ;ror eax, 1                  ; uprava na rotaci

        ;mov ecx, [ ebp + 12 ]       ; uprava
        ;or [ edx + ecx * 4 - 4 ], eax ; uprava

        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;

        global shl_intN
shl_intN:
        enter 0,0
        
        mov ecx, [ ebp + 12 ];      ; N
        mov edx, [ ebp + 8 ]        ; iN
        mov eax, 0                  ; inx
        clc

.back:
        rcl dword [ edx + eax * 4 ], 1
        inc eax
        loop .back

        mov eax, 0
        ;adc eax, 0
        setc al
        ;adc byte [ edx ], 0 ; uprava na rotaci

        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        global sub_intN_intN
sub_intN_intN:
        enter 0,0
        push ebx
        push esi

        mov edx, [ ebp + 8 ]        ; aN
        mov esi, [ ebp + 12 ]       ; bN
        mov ecx, [ ebp + 16 ]       ; N
        mov ebx, 0                  ; inx
        clc                         ; CF=0
.back:
        mov eax, [ esi + ebx * 4 ]
        sbb [ edx + ebx * 4 ], eax  ; aN[inx]+=bN[inx]
        inc ebx
        loop .back

        pop esi
        pop ebx

        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        global add_intN_intN
add_intN_intN:
        enter 0,0
        push ebx
        push esi

        mov edx, [ ebp + 8 ]        ; aN
        mov esi, [ ebp + 12 ]       ; bN
        mov ecx, [ ebp + 16 ]       ; N
        mov ebx, 0                  ; inx
        clc                         ; CF=0
.back:
        mov eax, [ esi + ebx * 4 ]
        adc [ edx + ebx * 4 ], eax  ; aN[inx]+=bN[inx]
        inc ebx
        loop .back

        pop esi
        pop ebx

        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        global mul_intN_int32
mul_intN_int32:
        enter 0,0
        push ebx
        push edi
        push esi
    
        mov ecx, [ ebp + 16 ]       ; N
        mov edi, [ ebp + 8 ]        ; intN
        mov ebx, 0
        mov esi, 0
.back:
        mov eax, [ edi + ebx  * 4 ]
        mul dword [ ebp + 12 ]
        
        add eax, esi
        adc edx, 0
        mov esi, edx

        mov [ edi + ebx * 4 ], eax

        inc ebx
        loop .back
                                    ; esi je preteceni
        pop esi
        pop edi
        pop ebx
        leave
        ret

        global add_intN_int32
add_intN_int32:
        enter 0,0
        push ebx
        push edi
    
        mov ecx, [ ebp + 16 ]       ; N
        mov edi, [ ebp + 8 ]        ; intN
        mov ebx, 1                  ; inx
        mov eax, [ ebp + 12 ]
        cdq
        mov eax, [ edi ]
        add eax, dword [ ebp + 12 ]
        mov [ edi ], eax            ; vysledek

        dec ecx
.back:
        jnc .taknic
        adc [ edi + ebx * 4 ], edx
        inc ebx
        loop .back
.taknic:
        pop edi
        pop ebx
        leave
        ret


        global div_intN_int32
div_intN_int32:
        enter 0,0
        push ebx
        push edi

        mov ecx, [ ebp + 16 ]       ; N
        mov ebx, [ ebp + 12 ]       ; int32
        mov edi, [ ebp + 8 ]        ; intN
        mov edx, 0
.back:
        mov eax, [ edi + ecx * 4 - 4 ]
        div ebx
        mov [ edi + ecx * 4 - 4 ], eax
        loop .back

        mov eax, edx

        pop edi
        pop ebx
        leave
        ret

        global div_i64_i32
div_i64_i32:
        enter 0,0
        push edi

        mov edx, 0
        mov eax, [ ebp + 12 ]       ; a_h
        div dword [ ebp + 16 ]       ; a_h /= b

        mov ecx, eax
                                    ; edx pokracuje
        mov eax, [ ebp + 8 ]        ; a_l
        div dword [ ebp + 16 ]      ; a_l /= b

        mov edi, [ ebp + 20 ]
        mov [ edi ], edx            ; zbytek

        mov edx, ecx                ; horni rad
                                    ; eax, dolni rad

        pop edi
        leave
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        global mul_i64_i32
mul_i64_i32:
        enter 0,0

        mov eax, [ ebp + 12 ]       ; a_h
        mul dword [ ebp + 16 ]      ; a_h *= b

        mov ecx, eax

        mov eax, [ ebp + 8 ]        ; a_l
        mul dword [ ebp + 16 ]      ; a_l *= b

        add edx, ecx

;        mov eax, [ ebp + 8 ]        ; a_l
;        mul dword [ ebp + 16 ]      ; a_l *= b
;
;        mov ecx, eax
;        mov edi, edx
;
;        mov eax, [ ebp + 12 ]       ; a_h
;        mul dword [ ebp + 16 ]      ; a_h *= b
;
;        add edi, eax
;        mov edx, edi
;        mov eax, ecx
;
        leave
        ret

