//***************************************************************************
//
// Program for education in subject "Assembly Languages"
// petr.olivka@vsb.cz, department of computer science, VSB-TUO
//
// Example of combining module in C with module in Assembly
//
//***************************************************************************

#include <stdio.h>
#include <string.h>
#include <strings.h>

int shl_intN( int *iN, int N );
int shr_intN( int *iN, int N );
void add_intN_intN( int *aN, int *bN, int N );
void sub_intN_intN( int *aN, int *bN, int N );

long long mul_i64_i32( long long a, int b );
long long div_i64_i32( long long a, int b, int *rem );
// intN = intN / int32
int div_intN_int32( int *intN, int int32, int N );
int add_intN_int32( int *intN, int int32, int N );
int mul_intN_int32( int *intN, int int32, int N );

void str_mirror( char *str )
{
    int l = strlen( str );
    char tmp[ l + 1 ];
    for ( int i = 0; i < l; i++ )
        tmp[ l - i - 1 ] = str[ i ];
    tmp[ l ] = 0;
    strcpy( str, tmp );
}

int is_zero( int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        if ( intN[ i ] ) return 0;

    return 1;
}

// compare of two intN numbers
// a >  b return  1
// a == b return  0
// a <  b return -1
int cmp_intNintN( int *a, int *b, int N )
{
  int i;
  for ( i = N - 1; i >= 0; i-- )
  {
      if ( a[ i ] == b[ i ] ) continue;
      if ( ( unsigned int ) a[ i ] > ( unsigned int ) b[ i ] ) return 1;
      return -1;
  }
  return 0;
}

void mul_intN_intN( int *aN, int *bN, int N )
{
    int res[ N ];
    bzero( res, sizeof( res ) );

    for ( int i = 0; i < N * 32; i++ )
    {
        if ( bN[ 0 ] & 1 )
        {
            add_intN_intN( res, aN, N );
        }
        shl_intN( aN, N );
        shr_intN( bN, N );
    }

    memcpy( aN, res, sizeof( res ) );
}

void div_intN_intN( int *aN, int *bN, int N )
{
    int divadlo[ 2 * N ];
    int res[ N ];
    bzero( res, sizeof( res ) );
    bzero( divadlo, sizeof( divadlo ) );
    memcpy( divadlo, aN, N * sizeof( int ) );
    int *scena = divadlo + N;

    for ( int i = 0; i < N * 32; i++ )
    {
        shl_intN( divadlo, 2 * N );
        shl_intN( res, N ); 
        if ( cmp_intNintN( scena, bN, N ) >= 0 )
        {
            res[ 0 ] |= 1;
            sub_intN_intN( scena, bN, N );
        }
    }
    memcpy( aN, res, N * sizeof( int ) );
    memcpy( bN, scena, N * sizeof( int ) ); // zbytek
}

int div_int32_int32( int a, int b )
{
    long long delenec = a;  // divadlo int[ 2 ]
    int *scena = ( int * ) &delenec;
    scena++;                // &int[ 1 ]
    int res = 0;

    for ( int i = 0; i < 32; i++ )
    {
        res <<= 1;
        delenec <<= 1;
        if ( *scena >= b )
        {
            res |= 1;
            *scena -= b;
        }

    }
    return res;
}

char *intN_to_str( int *intN, char *str, int N )
{
    char *tmp = str;
    while ( !is_zero( intN, N ) )
    {
        int zbytek = div_intN_int32( intN, 10, N );
        *str = zbytek + '0';
        str++;
    }
    *str = 0;

    str_mirror( tmp );
}

char *str_to_intN( char *str, int *intN, int N )
{
    for ( int i = 0; i < N; i++ )
        intN[ i ] = 0;
    while ( *str )
    {
        mul_intN_int32( intN, 10, N );
        add_intN_int32( intN, *str - '0', N );
        str++;
    }
}

int main()
{
    char sn1[ 100 ] = "123456789123456789123456789";
    char sn2[ 100 ] = "10000000000";
    char sres[ 100 ];

    int n1[ 8 ], n2[ 8 ];
    str_to_intN( sn1, n1, 8 );
    str_to_intN( sn2, n2, 8 );

    mul_intN_intN( n1, n2, 8 );
    intN_to_str( n1, sres, 8 );
    printf( "nasobeni %s\n", sres );

    str_to_intN( sn1, n1, 8 );
    str_to_intN( sn2, n2, 8 );
    div_intN_intN( n1, n2, 8 );
    intN_to_str( n1, sres, 8 );
    printf( "deleni %s\n", sres );
    intN_to_str( n2, sres, 8 );
    printf( "deleni %s\n", sres );

}

int mmain()
{
    int n1[ 8 ] = { 0xBEEFBEEF, 0x22222223, 0x44444445, 0x88888889, 0xCAFECAFE, 0x0, 0, 0 };
    int n2[ 8 ] = { 0x10000, 0, 0, 0, 0, 0, 0, 0 };
    //add_intN_intN( n1, n2, 8 );

    printf( "deleni %d\n", div_int32_int32( 10, 3 ) );

    div_intN_intN( n1, n2, 8 );

    for ( int i = 0; i < 8; i++ )
        printf( "%08X ", n1[ 7 - i ] );
    printf( "\n" );
    for ( int i = 0; i < 8; i++ )
        printf( "%08X ", n2[ 7 - i ] );
    printf( "\n" );
}
